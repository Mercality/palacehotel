/** function equalHeight(group) {    
tallest = 0;    
group.each(function() {       
thisHeight = $(this).height();       
if(thisHeight > tallest) {          
tallest = thisHeight;       
}    
});    
group.each(function() { $(this).height(tallest); });
} 



window.addEventListener('resize', function() {
setTimeout(function() {equalHeight($(".thumbnail"));}, 2000); }, 'false');
*/
function roundToTwo(num) {    
    return +(Math.round(num + "e+2")  + "e-2");
}

var navItems= document.getElementsByClassName('nav-hab-item');

for (var i = 0; i <= navItems.length-1; i++) {
	navItems[i].addEventListener('click', function(evt) {
		fotohab= document.getElementsByClassName('nav-hab-inner');
		var x = 1;
		var tipohab = evt.target.id;
		for(var i = 0; i <= fotohab.length-1; i++) {
          
			fotohab[i].src = "/images/habs/"+tipohab+"/"+tipohab+"_"+x+".gif";
			x++;
		}
	tns= document.getElementsByClassName('nav-hab-target');
	var y = 1;
	for(var j = 0; j <= tns.length-1; j++) {
        
		tns[j].src = "/images/habs/"+tipohab+"/tn_"+tipohab+"_"+y+".gif";
        y++;


	}}, 'false');

}
$(function(){
var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

}); //JQuery is loaded

 if ( document.getElementById('reservasHabita') != null ) {
 //Login Oculto
var loginOculto = document.getElementById('ocultoRes');
var siUserCheck = document.getElementById('siRegistrado');
var registroBut = document.getElementById('registroBut');
var noUserCheck = document.getElementById('noRegistrado');
if (siUserCheck) { 
  siUserCheck.addEventListener('change', function() {
	loginOculto.className = "";
  registroBut.className = "ocultoRes";
}, 'false');
}
if (noUserCheck) {
noUserCheck.addEventListener('change', function() {
	loginOculto.className = "ocultoRes";
  registroBut.className = "btn btn-primary";
  
}, 'false');
}
// Resize del select habitaciones
window.addEventListener('resize',function(){
	var selectHab = document.getElementsByClassName('listaha');

	for(i = 0; i <= selectHab.length-1; i++){
		if (window.innerWidth < 768){
			selectHab[i].size = 6;
		}
		if (window.innerWidth > 768) {
			selectHab[i].size = 1;
		}
	}
},'false');


//Resize del seelct habitaciones al cargar
window.addEventListener('load',function(){
	var selectHab = document.getElementsByClassName('listaha');

	for(i = 0; i <= selectHab.length-1; i++){
		if (window.innerWidth < 768){
			selectHab[i].size = 6;
		}
	}
},'false');

 //Boton Agregar Habitacion al Formulario
 var botonMas = document.getElementById('bMas');
 botonMas.addEventListener('click', function(){
 	var contarFormHab = document.getElementsByClassName("habitas");
 	var cantFormHab = contarFormHab.length+1;
 	contaH = cantFormHab.toString('Habitacion #'+contaH);
 	var campoHolder = document.getElementById('camposForm');
 	var spanHolder = document.getElementById('punteroHabs');
 	var habHolder = document.getElementById('habHolder');
 	var huesHolder = document.getElementById('huesHolder');
 	var punteroHues = document.getElementById('punteroHues');
 habHolder.innerHTML = '<div class="form-group habitas"><span class="form-horizontal col-sm-6 col-md-6"><label for="tipohabitacion[]">'+contaH+')</label><select name="tipohabitacion[]" id="tipohabitacion'+contaH+'" class="form-control listaha"><option value="Matrimonial">Matrimonial</option><option value="Matrimonial + Individual">Matrimonial + Individual</option><option value="3 Camas Individuales">3 Camas Individuales</option><option value="Doble Matrimonial">Doble Matrimonial</option><option value="4 Camas Individuales">4 Camas Individuales</option><option value="Suite Junior">Suite Junior</option><option value="Suite Palace">Suite Palace</option><option value="Suite Palace Familiar">Suite Palace Familiar</option></select></span><span class="form-horizontal col-sm-6 col-md-6 checkinout"><span class="col-md-12"><label for="checkin[]" class="control-label">DESDE:</label><input placeholder="Ej: 01/01/2014" style="" class="form-control calNoStyle dateIN" name="checkin[]" id="checkin'+contaH+'"></span><span class="col-md-12"><label for="checkout[]" class="control-label">HASTA:</label><input placeholder="Ej: 01/01/2014" style="" class="form-control calNoStyle dateOUT" name="checkout[]" id="checkout'+contaH+'"></span></span> <!-- Checkin and Out --></div>';
  huesHolder.innerHTML = '<div class="form-group"><label for="huesped[]" class="col-xs-4 col-sm-4 control-label">Huesped(es) Habitacion '+contaH+':</label><div class="col-xs-8 col-sm-8"><input type="text" class="form-control" name="huesped[]" id="huesped'+contaH+'" placeholder="ej: Juan Alfonzo y Maria Arteaga"></div></div> <!-- Huesped(es) -->';
   huesHolder.removeAttribute('id');
   var nuevoHuesHolder = document.createElement('div');
   nuevoHuesHolder.setAttribute('id','huesHolder');
   campoHolder.insertBefore(nuevoHuesHolder,punteroHues);

 	habHolder.removeAttribute('id');
 	var nuevoHolder = document.createElement('div');
 	nuevoHolder.setAttribute('id','habHolder');
 	campoHolder.insertBefore(nuevoHolder,spanHolder);

//Activar Calendario en inputs
//IN
     $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
       $('.dateIN').datepicker({
            minDate: 0,
            showButtonPanel: true,
            dateFormat : 'dd/mm/yy',
        });

//OUT
       $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
       $('.dateOUT').datepicker({
            
            minDate: 1,
            showButtonPanel: true,
            dateFormat : 'dd/mm/yy'
        });

$('#formReserva').bootstrapValidator('addField', 'huesped[]');
$('#formReserva').bootstrapValidator('addField', 'checkin[]');
$('#formReserva').bootstrapValidator('addField', 'checkout[]');

     },'false');

//Boton Eliminar Habitacion del Formulario
var botonMenos = document.getElementById('bMenos');
 botonMenos.addEventListener('click', function(){
 	var contarFormHab = document.getElementsByClassName("habitas");

 	if (contarFormHab.length <= 1){
 		return 0;
 	} else {
 	punteroRe = document.getElementById('habHolder');
 	divElim = punteroRe.previousSibling;
 	divElim.parentNode.removeChild(divElim);
    punteroHuesRe = document.getElementById('huesHolder');
    huesElim = punteroHuesRe.previousSibling;
    punteroHuesRe.parentNode.removeChild(huesElim);
 }
 },'false');

 //Cambiar formato del Teléfono $('#telefono').focusout(function(){
 	var probar = /^[0-9]{11}$/
 	if (probar.test($('#telefono').val())) {
 		var telefono = $('#telefono').val();
 		var nuevoTlf = new String();
 		var x = 0;
        for (var i = 0; i <= telefono.length-1;i++){
        	if (i == 3  || i == 6){
        		nuevoTlf = nuevoTlf.concat(telefono[i]);
        		nuevoTlf = nuevoTlf.concat('-');
        		x++;
        		x++;
        	} else {
        		nuevoTlf = nuevoTlf.concat(telefono[i]);
        		x++;
        	}
        }
 		$('#telefono').val(nuevoTlf);
 	}
 });

 //Dropdown de cedula
$('#Ven').click(function(){
	$('#tipoCed').html('V <span class="caret"></span>');
	$('#tipoCedu').val('V');
});
$('#Ex').click(function(){
	$('#tipoCed').html('E <span class="caret"></span>');
	$('#tipoCedu').val('E');
});
$('#Jud').click(function(){
	$('#tipoCed').html('J <span class="caret"></span>');
    $('#tipoCedu').val('J');
});
$('#Gob').click(function(){
	$('#tipoCed').html('G <span class="caret"></span>');
    $('#tipoCedu').val('G');
});
$('#Pas').click(function(){
  $('#tipoCed').html('P <span class="caret"></span>');
    $('#tipoCedu').val('P');
});

//Scripts del modal de reserva
//Tarifas de habitacion
 var   costoMat = 580.36,
       costoMat1 = 625.00,
       costo3Ind = 758.93,
       costoDobMat = 803.57,
       costo4Ind = 803.57,
       costoSuiteJ = 892.86,
       costoSuiteP = 982.14,
       costoSuitePF = 1071.43;
//Activar Calendario en inputs
//IN
     $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
       $('.dateIN').datepicker({
            
            minDate: 0,
            showButtonPanel: true,
            dateFormat : 'dd/mm/yy'
        });
//OUT
       $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
       $('.dateOUT').datepicker({
            
            minDate: 1,
            showButtonPanel: true,
            dateFormat : 'dd/mm/yy'
        });

$('#submitModal').click(function(){
	//Validar antes de mostrar el modal
    $('#formReserva').data('bootstrapValidator').validate();
    if (!$('#formReserva').data('bootstrapValidator').isValid()){
    } else {
	//Recuperar datos del formulario
	var tdBase = document.getElementById('baseI'),
	    tdiva = document.getElementById('iva'),
	    tdtotalP = document.getElementById('totalPagar'),
		solicitante = $('#solicitante').val(),
    nombre_rz = $('#nombre_rz').val(), 
		email = $('#email').val(),
		telefono = $('#telefono').val(),
		cedula = $('#cedrif').val(),
		tipoCedu = $('#tipoCedu').val(),
    comentar = $('#Comentarios').val(),
		habs = document.getElementsByClassName('habitas'),
		cant = habs.length;
		cedula = tipoCedu.concat(cedula),
		total = 0,
//Referencia de la tabla para insercion
		tabla = document.getElementById('tablaConfRes');
//Limpiar tabla antes de cargarla
    while (tabla.lastChild.id != 'lish') {
    	tabla.removeChild(tabla.lastChild);
    }
    while (tdBase.lastChild.className != 'noElim') {
    	tdBase.removeChild(tdBase.lastChild);
    }
    while (tdiva.lastChild.className != 'noElim') {
    	tdiva.removeChild(tdiva.lastChild);
    }
    while (tdtotalP.lastChild.className != 'noElim') {
    	tdtotalP.removeChild(tdtotalP.lastChild);
    }

//Insertar en tabla datos del solicitante
  $('#nrz').html(nombre_rz);
  $('#nrz').attr('class', 'fconsola');
	$('#sol').html(solicitante);
  $('#sol').attr('class', 'fconsola');
	$('#ema').html(email);
  $('#ema').attr('class', 'fconsola');
	$('#tel').html(telefono);
  $('#tel').attr('class', 'fconsola');
	$('#cr').html(cedula);
  $('#cr').attr('class', 'fconsola');
  $('#comenta').html(comentar);
  $('#comenta').attr('class', 'fconsola');
//Contar Habitaciones
	for (var i = 0; i <= habs.length-1; i++) {
//Obtener informacion de las habitaciones solicitadas
		var checkin1 = $('#checkin'+(i+1)).val(),
		checkout1 = $('#checkout'+(i+1)).val(),
		tipohabit = $('#tipohabitacion'+(i+1)).val(),
		nomHuesped = $('#huesped'+(i+1)).val();
    checkout1 = moment(checkout1,'DD-MM-YYYY');
    checkin1 = moment(checkin1,'DD-MM-YYYY');
//Dar formato a la fecha que ira en la tabla
		var checkout = checkout1.format('DD/MM/YYYY');
		var checkin = checkin1.format('DD/MM/YYYY');
//Determinar numero de noches entre las dos fechas
		var noches = checkout1.diff(checkin1,'Days');
        var subtotal  = 0.00,
            costohab;
        switch(tipohabit){
        	case 'Matrimonial':
              costohab = costoMat;
              tipohabit = 'Matrimonial'
              break;
            case 'Matrimonial + Individual':
              costohab = costoMat1;
              tipohabit = 'Matrimonial con Individual'
              break;
              case '3 Camas Individuales':
              costohab = costo3Ind;
              tipohabit = '3 Camas Matrimoniales'
              break;
              case 'Doble Matrimonial':
              costohab = costoDobMat;
              tipohabit = 'Doble Matrimonial'
              break;
              case '4 Camas Individuales':
              costohab = costo4Ind;
              tipohabit = 'Cuatro Camas Individuales'
              break;
              case 'Suite Junior':
              costohab = costoSuiteJ;
              tipohabit = 'Suite Junior'
              break;
              case 'Suite Palace':
              costohab = costoSuiteP;
              tipohabit = 'Suite Palace'
              break;
              case 'Suite Palace Familiar':
              costohab = costoSuitePF;
              tipohabit = 'Suite Palace Familiar'
              break;
        }
       subtotal = roundToTwo(costohab*noches);
       total = roundToTwo(total+subtotal);
       subtotal = accounting.formatMoney(subtotal, [symbol = ""], [precision = 2], [thousand = "."], [decimal = ","], [format = "%s%v"]);
//Crear Elementos vacios
		var nHabrows = document.createElement('tr'),
		nHab = document.createElement('td'),
		nIn = document.createElement('td'),
		nOut = document.createElement('td'),
		nNoches = document.createElement('td'),
		nHuesNom = document.createElement('td'),
		nSubtotal = document.createElement('td'),
    inputNoches= document.createElement('input');
//Crear nodos de texto
		var nHabi = document.createTextNode(tipohabit),
		textonHuesNom = document.createTextNode(nomHuesped),
		nFecInVal = document.createTextNode(checkin),
		nFecOutVal = document.createTextNode(checkout),
		nochesText = document.createTextNode(noches),
		subtotalText = document.createTextNode(subtotal);
//Unir nodos de texto a Elementos vacios
        nSubtotal.setAttribute('class','montos');
        inputNoches.setAttribute('type','hidden');
        inputNoches.setAttribute('name','noches[]');
        inputNoches.setAttribute('value',noches);
        nHab.appendChild(nHabi);
        nHab.setAttribute('class', 'fconsola');
        nHuesNom.appendChild(textonHuesNom);
        nHuesNom.setAttribute('class', 'fconsola');
        nIn.appendChild(nFecInVal);
        nIn.setAttribute('class', 'fconsola');
        nOut.appendChild(nFecOutVal);
        nOut.setAttribute('class', 'fconsola');
        nNoches.appendChild(nochesText);
        nNoches.setAttribute('class', 'fconsola text-center');
        nNoches.appendChild(inputNoches);
        nSubtotal.appendChild(subtotalText);
//Unir Elementos en la fila de la habitacion
        nHabrows.appendChild(nHab);
        nHabrows.appendChild(nHuesNom);
        nHabrows.appendChild(nIn);
        nHabrows.appendChild(nOut);
        nHabrows.appendChild(nNoches);
        nHabrows.appendChild(nSubtotal);
//Unir la fila a la tabla    
        tabla.appendChild(nHabrows);
//Crear un id a cada habitacion
		nHabrows.setAttribute('id', 'hab'+(i+1));

}	
	var iva = roundToTwo(total*12/100),
	    totalF = total+iva,
	    nuevoBi = document.createElement('td'),
	    nuevoIva = document.createElement('td'),
	    nuevoTotal = document.createElement('td'),
	    arrayTot = [total,iva,totalF],
	    arrayTot = accounting.formatColumn(arrayTot, [symbol = "Bs     "], [precision = 2], [thousand = "."], [decimal = ","], [format = "%s%v"]);
 
	var textoBi = document.createTextNode(arrayTot[0]),
	    textoIva = document.createTextNode(arrayTot[1]),
	    textoTotal = document.createTextNode(arrayTot[2]);
	nuevoBi.setAttribute('class','montos');
	nuevoIva.setAttribute('class','montos');
	nuevoTotal.setAttribute('class','montos');

	nuevoBi.appendChild(textoBi);
	nuevoIva.appendChild(textoIva);
	nuevoTotal.appendChild(textoTotal);
    tdBase.appendChild(nuevoBi);
    tdiva.appendChild(nuevoIva);
    tdtotalP.appendChild(nuevoTotal);

  
	$('#myModal').modal(); 
	$('#myModal').show;
  $('#submitBut').removeAttr('disabled');
}});
}





      //Cambiar formato del Teléfono $('#telefono').focusout(function(){
  var probar = /^[0-9]{11}$/
  var probarGuion = /\-+/
     if ($('#telefono').val().length > 13) {
    var valor,
        nuevoVal;
    valor = $('#telefono').val();
    if (probarGuion.test(valor)) {
      nuevoVal = valor.substr(0,13);
    }
    else {
    nuevoVal = valor.substr(0,11);
  }
    $('#telefono').val(nuevoVal);
   }
  if (probar.test($('#telefono').val())) {
    var telefono = $('#telefono').val();
    var nuevoTlf = new String();
    var x = 0;
        for (var i = 0; i <= telefono.length-1;i++){
          if (i == 3  || i == 6){
            nuevoTlf = nuevoTlf.concat(telefono[i]);
            nuevoTlf = nuevoTlf.concat('-');
            x++;
            x++;
          } else {
            nuevoTlf = nuevoTlf.concat(telefono[i]);
            x++;
          }
        }
    $('#telefono').val(nuevoTlf);
  }

 });


$(window).load(function() {

$('#formReserva').removeClass('hidden');
$('#formLogin').removeClass('hidden');
$('#loading').remove();
$('#smallModal').modal('show');


});

 if ( document.getElementById('registro') != null ) {
$('#Ven').click(function(){
  $('#tipoCed').html('V <span class="caret"></span>');
  $('#tipoCedu').val('V');
});
$('#Ex').click(function(){
  $('#tipoCed').html('E <span class="caret"></span>');
  $('#tipoCedu').val('E');
});
$('#Jud').click(function(){
  $('#tipoCed').html('J <span class="caret"></span>');
    $('#tipoCedu').val('J');
});
$('#Gob').click(function(){
  $('#tipoCed').html('G <span class="caret"></span>');
    $('#tipoCedu').val('G');
});
$('#Pas').click(function(){
  $('#tipoCed').html('P <span class="caret"></span>');
    $('#tipoCedu').val('P');
});
 }