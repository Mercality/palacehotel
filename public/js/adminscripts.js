$(window).load(function() {
    $(".rechazar").click(function(a) {
        a.preventDefault(), $("#bSi").attr("href", a.currentTarget.href), 
        $("#textoConfirmacion").html('¿Está seguro que desea rechazar esta reserva?<br>Motivo:<br><input type="text" size="50" id="motivo" name="motivo"> '), 
        $("#tituloConfirmacion").html("Rechazar Reserva"), $("#smallModal2").modal("show")
        
    }), 

    $(".aprobar").click(function(a) {
        a.preventDefault(), $("#bSi").attr("href", a.currentTarget.href),
        $("#textoConfirmacion").html("Esta seguro que desea aprobar esta reserva?"),
        $("#tituloConfirmacion").html("Aprobar Reserva"), $("#smallModal2").modal("show")
    }),

    $(document).on('change', '#motivo', function(a) {
    	$("#bSi").attr('href',$("#bSi").attr('href') + '&motivo=' + $("#motivo").val() );
    })
});

$(document).on('change', '.checkbox', function(e) {
    var id = e.target.id;
    id = id.substr(7, 7);
    $('#tarifanoc' + id).val(0);
    $('#tarifanoc' + id).attr('name', 'corporativa[' + id + ']');
    $('#tarifac' + id).val(1);
});