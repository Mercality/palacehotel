<?php

class RegistroController extends BaseController
{
    /*
|--------------------------------------------------------------------------
| Pagina de Registro de Usuario
|--------------------------------------------------------------------------
| 
| return View
| 
*/

  public function getRegistro()
  {
      return View::make('reservas.registro')->with('editar', false);
  }

/*
|--------------------------------------------------------------------------
| Crear Usuario
|--------------------------------------------------------------------------
| 
| return View
| 
*/
  public function postCreate()
  {

//Capturar datos de usuario
        $usuario = array(
            'identificacion' => Input::get('usuario'),
            'password' => Input::get('password'),
            'tipo' => 'cliente',
            'email' => Input::get('email'),
            );

//Validar Usuario
        $vU = Usuario::validate($usuario);
      if ($vU !== true) {
          return 'usuario invalido';
      } else {
          //Crear nuevo Usuario
            $usuario['password'] = Hash::make($usuario['password']);
          $usuario = Usuario::create($usuario);
          $cliente = array(
                'nombre_rz' => Input::get('nombre_rz'),
                'ced_rif' => trim(Input::get('tipoCedu')).trim(Input::get('cedrif')),
                'telefono' => Input::get('telefono'),
                'email' => Input::get('emailC'),
                'usuario_id' => $usuario->id,
                );
//Validar Cliente
            $vC = Cliente::validate($cliente);
          if ($vC !== true) {
              Usuario::destroy($usuario->id);

              return 'cliente invalido';
          } else {
              //Crear cliente
                if ($clienteExiste = Cliente::whereCed_rif($cliente['ced_rif'])->first()) {
                    $clienteExiste->update($cliente);
                } else {
                    $cliente = Cliente::create($cliente);
                }
//Verificar si empresa
//Iterar sobre nombre[] y agregar al array	
          }

          return Redirect::to('reserva');
      }
  }
/*
|--------------------------------------------------------------------------
| Pagina de Edicion de Usuario
|--------------------------------------------------------------------------
| 
| return View
| 
*/
  public function getEditar()
  {
      if (isset(Auth::user()->id)) {
          $id = Auth::user()->id;
      } else {
          $id = null;
      }
      $usuario = Usuario::with('Cliente')->findOrFail($id);

      return View::make('reservas.editar_perfil')->with(array('usuario' => $usuario,
                                                            'cliente' => $usuario->cliente,
                                                            'editar' => true, ));
  }
/*
|--------------------------------------------------------------------------
| Editar Usuario
|--------------------------------------------------------------------------
| 
| return View
| 
*/
  public function updateEditar()
  {

//Capturar datos de usuario
        $usuario = Usuario::with('Cliente')->find(Auth::user()->id);
      if (Input::has('password')) { $usuario->password = Hash::make(Input::get('password')); }
      $usuario->email = Input::get('email');

//Validar Usuario
        $vU = Usuario::validateEdit($usuario->toArray(), $usuario->id);
      if ($vU !== true) {
          return ('Usuario Invalido');
      } else {
          //Actualizar Usuario
            $usuario->save();
//Obtener Datos de Cliente
            $cliente = $usuario->cliente;
          $cliente->nombre_rz = Input::get('nombre_rz');
          $cliente->ced_rif = Input::get('tipoCedu').Input::get('cedrif');
          $cliente->telefono = Input::get('telefono');
          $cliente->email = Input::get('emailC');

//Validar Cliente
            $vC = Cliente::validate($cliente->toArray());
          if ($vC !== true) {
              return 'cliente invalido';
          } else {
              //Actualizar
            $cliente->save();
//Verificar si empresa
//Iterar sobre nombre[] y agregar al array	
          }

          return Redirect::to('reserva');
      }
  }
}
