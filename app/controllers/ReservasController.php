<?php

class ReservasController extends BaseController
{
    /*
|--------------------------------------------------------------------------
| Muestra la pagina de inicio de las reservas
|--------------------------------------------------------------------------
| 
| Return Vista con datos del cliente
| 
*/

  public function showIndex()
  {
      $habitaciones = Habitacion::all();
      if (Auth::check() == false) {
          return View::make('reservas.reservas')->with('habitaciones', $habitaciones);
      } else {
          $cliente = Cliente::whereUsuario_id(Auth::id())->first();

          return View::make('reservas.reservas')->with(array('cliente' => $cliente,
                                                         'habitaciones' => $habitaciones, ));
      }
  }

/*
|--------------------------------------------------------------------------
| Procesar Inicio de Sesion
|--------------------------------------------------------------------------
| 
| return Redirect
| 
*/

  public function postLogin()
  {
      $habitaciones = Habitacion::all();
      if (Input::has('identificacion') && Input::has('password')) {
          $identificacion = Input::get('identificacion');
          $password = Input::get('password');

          if ((Auth::attempt(array('identificacion' => $identificacion, 'password' => $password)))) {
              return Redirect::action('ReservasController@showIndex');
          } else {
              return Redirect::to('reserva')->with(array('errores' => array('loginfail' => true),
                                             'habitaciones' => $habitaciones, ));
          }
      }
      else {
        return Redirect::to('reserva')->with(array('errores' => array('loginfail' => true),
                                             'habitaciones' => $habitaciones, ));
      }
  }


/*
|--------------------------------------------------------------------------
| Procesar Cierre de Sesion
|--------------------------------------------------------------------------
| 
| Description
| 
*/
  public function getLogout()
  {
      Auth::logout();

      return Redirect::to('reserva');
  }
/*
|--------------------------------------------------------------------------
| Crear una nueva reserva
|--------------------------------------------------------------------------
| 
| return Redirect
| 
*/

  public function postCreate()
  {
      $time = new DateTime();
//Esta funcion crea una  instancia de Solicitud y la asigna a la variable del mismo nombre.
    $solicitud = Reserva::crearSolicitud();
//Se verifica si hay un usuario con sesion iniciada para crear la reserva a su nombre
    if (Auth::check()) {
        $usuario = Auth::user();
        $cliente = Cliente::whereUsuario_id($usuario->id)->first();
        $solicitud->cliente_id = $cliente->id;
        $solicitud->save();
    }
//Si no hay una sesion iniciada se crea un nuevo cliente no registrado con los datos ingresados
    else {
        $cedularif = trim(Input::get('tipoCedu')).trim(Input::get('cedrif'));
        $invitado = Cliente::whereNombre_rz('Invitado')->first();
//Verificar si el rif existe en la Base de Datos
      if ($cliente = Cliente::whereCed_rif($cedularif)->first()) {
          $verUsuario = Usuario::whereRaw('id = ? and id != ?', array($cliente->usuario_id, $invitado->id))->first();
//Si el rif pertenece a un usuario registrado solicitar inicio de sesión
        if ($verUsuario) {
            return Redirect::to('reserva')->with('errores', array(
                                              'registrado' => array(
                                                            'cedula' => $cliente->ced_rif, ), ));
        }
//Si no pertenece a un usuario registrado proceder con la solicitud
        else {
            $usuario = Usuario::whereIdentificacion('Invitado')->first();
            $cliente = Reserva::actualizarCliente($usuario, $cliente);
            $solicitud->cliente_id = $cliente->id;
            $solicitud->save();
        }
      }
//Si no está registrado, usar el usuario Invitado y proceder con la solicitud
      else {
          $usuario = Usuario::whereIdentificacion('Invitado')->first();
          $cliente = Reserva::crearCliente($usuario);
          $solicitud->cliente_id = $cliente->id;
          $solicitud->save();
      }
    }
//Guardar las reservas por habitación, calcular costo total(base) y guardar la solicitud.
    $habs = Reserva::iterarHabs($solicitud);
      $solicitud->costo_total = $habs['total'];
      $solicitud->save();
      $gtotal = $solicitud->costo_total + $habs['iva'];
//Generar archivo pdf, retorna el nombre del archivo.
    //$nombreAr = Reserva::crearPdf($cliente, $solicitud, $habs['habsHtml'], $time, $habs['iva'], $gtotal, 'save');
      $nombreAr = null;
//Envía el archivo pdf creado por correo 
    Reserva::procesarMail($solicitud, 'Reserva', 'Solicitud de Reserva de Habitacion N°'.$solicitud->numeroSolicitud());
    Reserva::procesarMail($solicitud, 'Nueva_Reserva', 'Cliente: '.$solicitud->cliente->nombre_rz.' Solicitud N°'.$solicitud->numeroSolicitud());

      return Redirect::to('/')->with('reserva', 'success');
  }
}
