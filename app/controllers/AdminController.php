<?php

class AdminController extends BaseController
{
    /*
|--------------------------------------------------------------------------
| Mostrar Index el Panel Administrativo
|--------------------------------------------------------------------------
| 
| Carga la pagina principal del panel con información de nuevas reservas y usuarios.
| @return View
| 
*/
    public function showIndex()
    {
        $reservas = Reserva::orderBy('created_at', 'desc')->limit('10')->with('Habitacion')->get();
        $usuarios = Usuario::all()->count();
        $cantidadSolicitudes = Solicitud::whereConfirmada(null)->count();
        $visitas = $this->visitasHoy();

        return View::make('admin.index')->with(array(
                                'reservas' => $reservas,
                                'usuarios' => $usuarios,
                                'cantidadSolicitudes' => $cantidadSolicitudes,
                                'visitas' => $visitas, ));
    }
/*
|--------------------------------------------------------------------------
| Mostrar lista de usuarios Registrados
|--------------------------------------------------------------------------
| 
| Muestra una lista de los usuarios registrados y permite manipular la
| informacion de los mismos
| @return View
*/
    public function showUserIndex()
    {
        $usuarios = Usuario::with('Cliente')->orderBy('tipo', 'asc')->orderBy('created_at', 'asc')->orderBy('id', 'asc')->paginate(10);

        return View::make('admin.usuarios.index')->with('usuarios', $usuarios);
    }

    public function showEditarUser()
    {
        $usuarios = Usuario::with('Cliente')->orderBy('tipo', 'asc')->orderBy('created_at', 'asc')->orderBy('id', 'asc')->paginate(10);

        return View::make('admin.usuarios.editar')->with('usuarios', $usuarios);
    }
    public function putEditarUser()
    {
        $tCorporativa = Input::get('corporativa');
        $tipoUsuario = Input::get('tipo');
        foreach ($tCorporativa as $id => $valor) {
            if ($valor === '1' || $valor === '0') {
                Cliente::whereId($id)->update(['tarifa_corporativa' => $valor]);
            };
        }

        foreach ($tipoUsuario as $id => $valor) {
            if ($valor === 'admin' || $valor === 'cliente') {
                Usuario::whereId($id)->update(['tipo' => $valor]);
            };
        }

        return Redirect::to('admin/usuarios/');
    }
/*
|--------------------------------------------------------------------------
| Muestra la lista de las reservas pendientes / Aprobadas / Rechazadas
|--------------------------------------------------------------------------
| 
| @Return View
| 
*/
    public function showReservaIndex()
    {
        $ver = Input::get('ver');
        if ($ver == 'nuevas') {
            $solicitudes = Solicitud::whereConfirmada(null)->orderBy('created_at', 'desc')->with('reserva.habitacion')->paginate(10);
            $solicitudes->appends(array('ver' => 'nuevas'));
        }
        if ($ver == 'aprobadas') {
            $solicitudes = Solicitud::whereConfirmada(true)->orderBy('created_at', 'desc')->with('reserva.habitacion')->paginate(10);
            $solicitudes->appends(array('ver' => 'aprobadas'));
        }
        if ($ver == 'rechazadas') {
            $solicitudes = Solicitud::whereConfirmada(false)->orderBy('created_at', 'desc')->with('reserva.habitacion')->paginate(10);
            $solicitudes->appends(array('ver' => 'rechazadas'));
        }
        if ($ver == 'todas' || !isset($ver)) {
            $solicitudes = Solicitud::orderBy('created_at', 'desc')->with('reserva.habitacion')->paginate(10);
            $solicitudes->appends(array('ver' => 'todas'));
        }
        $nuevasReservas = Solicitud::whereConfirmada(null)->count();
        $confirmadas = Solicitud::whereConfirmada(true)->count();
        $rechazadas = Solicitud::whereConfirmada(false)->count();
        $todas = Solicitud::all()->count();

        return View::make('admin.reservas.index')->with(array(
                                                    'solicitudes' => $solicitudes,
                                                    'nuevasReservas' => $nuevasReservas,
                                                    'confirmadas' => $confirmadas,
                                                    'rechazadas' => $rechazadas,
                                                    'todas' => $todas, ));
    }
/*
|--------------------------------------------------------------------------
| Mostrar PDF de la Reserva
|--------------------------------------------------------------------------
| 
| Genera y muestra en linea el PDF de una reserva existente
| @return PDF->Stream()
| 
*/
    public function showPdf($id)
    {
        if ($solicitud = Solicitud::whereId($id)->with('cliente')->with('reserva.habitacion')->first()) {
        $time = new DateTime();
        $habs = Reserva::genHabs($solicitud);
        $solicitud->costo_total = $habs['total'];
        $gtotal = $solicitud->costo_total + $habs['iva'];

        return Reserva::crearPdf($solicitud->cliente, $solicitud, $habs['habsHtml'], $time, $habs['iva'], $gtotal, 'stream');
        } else {
            App::abort(404);
        }
    }
/*
|--------------------------------------------------------------------------
| Confirmar o Rechazar Reserva
|--------------------------------------------------------------------------
| 
| Se confirman o se rechazan las reservas retornando un mensaje indicando la acción realizada
| @return Redirect con mensaje
| 
*/
    public function confirmarReserva($id)
    {
        
        $confirmacion = Input::get('conf');
        $solicitud = Solicitud::whereId($id)->first();

        if ($confirmacion == 'si') {
            $time = new DateTime();
            $habs = Reserva::genHabs($solicitud);
            $gtotal = $solicitud->costo_total + $habs['iva'];
            $nombreAr = Reserva::crearPdf($solicitud->cliente, $solicitud, $habs['habsHtml'], $time, $habs['iva'], $gtotal, 'save');

            $solicitud->confirmada = true;
            $solicitud->save();
            $data = array('solicitud' => $solicitud);
            Reserva::procesarMail($solicitud, 'Aprobada', 'Palace Hotel, C.A.: Reserva Aprobada N°'.$solicitud->numeroSolicitud(), $nombreAr);

            return Redirect::to('admin/reservas?ver=nuevas')->with(array(
                                                        'confirmada' => 'si',
                                                        'id' => $solicitud->id, ));
        } elseif ($confirmacion == 'no') {
            $solicitud->confirmada = false;
            $solicitud->comentarios = Input::get('motivo');
            $solicitud->save();
            $data = array('solicitud' => $solicitud);
            Reserva::procesarMail($solicitud, 'Rechazada', 'Palace Hotel, C.A.: Reserva Cancelada/Rechazada N°'.$solicitud->numeroSolicitud());

            return Redirect::to('admin/reservas?ver=nuevas')->with(array(
                                                        'confirmada' => 'no',
                                                        'id' => $solicitud->id, ));
        } else {
            
            return Redirect::to('admin/reservas')->with('confirmada', 'error');
        }

        return Redirect::to('admin/reservas')->with('confirmada', 'error');
    }
/*
|--------------------------------------------------------------------------
| Mostrar Estadisticas de Visitas
|--------------------------------------------------------------------------
| 
| @return View
| 
*/
    public function showEstadisticasIndex()
    {
        $visitors = Visitor::orderBy('created_at', 'desc')->paginate(30);
        return View::make('admin.estadisticas.index')->with(array(
                                                            'visitors' => $visitors
                                                            ));
    }
/*
|--------------------------------------------------------------------------
| Mostrar editor de precios de habitaciones
|--------------------------------------------------------------------------
| 
| Lista de habitaciones con campos para editar precios
| 
*/
    public function showHabitacionesIndex()
    {
        $habitaciones = Habitacion::all();

        return View::make('admin.habitaciones.index')->with(array('habitaciones' => $habitaciones));
    }
/*
|--------------------------------------------------------------------------
| Editar Precios de Habitaciones
|--------------------------------------------------------------------------
| 
| Se ingresan los nuevos precios, se validan y se actualizan en la BDD
| @return Redirect
| 
*/
    public function updateHabitaciones()
    {
        $nuevasTarifas = Input::get('nuevaTarifa');
        $nuevasTarifasC = Input::get('nuevaTarifaC');
        $errores = false;
        $habitaciones = false;

        foreach ($nuevasTarifas as $id => $precio) {
            $precioC = $nuevasTarifasC[$id];
            $vH = Habitacion::validate(array('tarifa' => $precio));
            $vH2 = Habitacion::validate(array('tarifa' => $precioC));
            if ($precio != '' || $precioC != '') {
                $habitacion = Habitacion::find($id);
                if ($vH !== true) {
                    $errores = array('errores' => 'validacion');
                    $habitaciones[$habitacion->id] = $habitacion->tipo;
                } else {
                    $habitacion->tarifa = str_replace(',', '.', $precio);
                    $habitacion->save();
                }
                if ($vH2 !== true) {
                    $errores = array('errores' => 'validacion');
                    $habitaciones[$habitacion->id] = $habitacion->tipo;
                } else {
                    $habitacion->tarifa_corporativa = str_replace(',', '.', $precioC);
                    $habitacion->save();
                }
            }
        }

        if (!$errores) {
            $usuarios = Usuario::where('Identificacion','<>','Invitado')->with('cliente')->get();
            $clienteNormal = Array();
            $clienteCorporativo = Array();
            foreach($usuarios as $usuario) {
                if($usuario->cliente->tarifa_corporativa) {
                    array_push($clienteCorporativo, $usuario->cliente->email);
                }
                if(!$usuario->cliente->tarifa_corporativa) {
                    array_push($clienteNormal, $usuario->cliente->email);
                }
            }
            if ($clienteNormal) {
                Reserva::procesarMail(null, 'Tarifas', 'Palace Hotel, C.A.: Cambio de Tarifas', null, $clienteNormal);
            }
            if ($clienteCorporativo) {
                Reserva::procesarMail(null, 'Tarifas_Corporativas', 'Palace Hotel, C.A.: Cambio de Tarifas', null, $clienteCorporativo);
            }
        } else {
            return Redirect::to('admin/habitaciones')->with(array('errores' => $errores,
                                                              'habitacion' => $habitaciones, ));
        }


        return Redirect::to('admin/habitaciones')->with(array('errores' => $errores,
                                                              'habitacion' => $habitaciones, ));
    }

    public function visitasHoy()
    {
        $visitors = Visitor::all();
        $date = Carbon\Carbon::now();
        $visitantes = 0;
        foreach ($visitors as $visitor) {
            if ($visitor->updated_at->toDateString() === $date->toDateString()) {
                $visitantes = $visitantes + 1;
            }
        }
        return $visitantes;
    }
}
