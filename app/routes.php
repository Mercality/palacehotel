<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*REDIRECCIONES*/
Route::get('imformacion', function () {return Redirect::to('/#qs',301); });
Route::get('reservaciones', function () {return Redirect::to('/reserva',301); });
Route::get('contacto', function () {return Redirect::to('/#Contacto',301); });
/*REDIRECCIONES*/

/*RUTAS HABITACIONES*/
Route::get('habitaciones/matrimonial', function () {return View::make('habitaciones.matrimonial')->with('precio', Habitacion::find(1)->tarifa); });
Route::get('habitaciones/matInd', function () {return View::make('habitaciones.matInd')->with(array('precio' => Habitacion::find(2)->tarifa, 'precio2' => Habitacion::find(3)->tarifa)); });
Route::get('habitaciones/triple', function () {return View::make('habitaciones.triple')->with('precio', Habitacion::find(4)->tarifa); });
Route::get('habitaciones/IVDobMAT', function () {return View::make('habitaciones.IVDobMAT')->with('precio', Habitacion::find(5)->tarifa); });
Route::get('habitaciones/IV2MAT2IND', function () {return View::make('habitaciones.IV2MAT2IND')->with('precio', Habitacion::find(6)->tarifa); });
Route::get('habitaciones/IV4IND', function () {return View::make('habitaciones.IV4IND')->with('precio', Habitacion::find(7)->tarifa); });
Route::get('habitaciones/Junior', function () {return View::make('habitaciones.Junior')->with('precio', Habitacion::find(8)->tarifa); });
Route::get('habitaciones/Palace', function () {return View::make('habitaciones.Palace')->with('precio', Habitacion::find(9)->tarifa); });
Route::get('habitaciones/PalaceFamiliar', function () {return View::make('habitaciones.PalaceFamiliar')->with('precio', Habitacion::find(10)->tarifa); });
/*FIN RUTAS HABITACIONES*/

/* Navegacion del Menu */
Route::get('/', function () { return View::make('index'); });
Route::get('servicios', function () { return View::make('servicios'); });
Route::get('habitaciones', function () { return View::make('habitaciones'); });
Route::get('tascarestaurant', function () { return View::make('tascarestaurant'); });
/******************************************************************************************************/

/* Pagina de Reserva */
Route::get('reserva', 'ReservasController@showIndex');
Route::post('reserva/login', 'ReservasController@postLogin');
Route::post('reserva/reservar', 'ReservasController@postCreate');
Route::get('logout', 'ReservasController@getLogout');
/******************************************************************************************************/

/* Registro y Edición de Usuarios */
Route::get('usuario/registro', 'RegistroController@getRegistro');
Route::post('usuario/registro', 'RegistroController@postCreate');
Route::get('usuario/editar', 'RegistroController@getEditar');
Route::put('usuario/{id}', 'RegistroController@updateEditar');
/******************************************************************************************************/

/* Panel Administrativo */
Route::get('admin',array('before' => 'auth.admin', 'uses' => 'AdminController@showIndex'));
Route::get('admin/usuarios',array('before' => 'auth.admin', 'uses' => 'AdminController@showUserIndex'));
Route::get('admin/usuarios/editar',array('before' => 'auth', 'uses' => 'AdminController@showEditarUser'));
Route::put('admin/usuarios',array('before' => 'auth.admin', 'uses' => 'AdminController@putEditarUser'));
Route::get('admin/reservas',array('before' => 'auth.admin', 'uses' => 'AdminController@showReservaIndex'));
Route::get('admin/reservas/mostrarR/{id}',array('before' => 'auth.admin', 'uses' => 'AdminController@showPdf'));
Route::get('admin/reservas/confirmar/{id}',array('before' => 'auth.admin', 'uses' => 'AdminController@confirmarReserva'));
Route::get('admin/estadisticas',array('before' => 'auth.admin', 'uses' => 'AdminController@showEstadisticasIndex'));
Route::get('admin/habitaciones',array('before' => 'auth.admin', 'uses' => 'AdminController@showHabitacionesIndex'));
Route::post('admin/habitaciones',array('before' => 'auth.admin', 'uses' => 'AdminController@updateHabitaciones'));
/******************************************************************************************************/

Route::get('password', 'RemindersController@getRemind');
Route::post('password', 'RemindersController@postRemind');
Route::get('password/reset/{token}', 'RemindersController@getReset');
Route::post('password/reset', 'RemindersController@postReset');

/* Pruebas de funciones */
Route::get('testing', function () {
$usuario = Usuario::with('Cliente')->orderBy('tipo', 'asc')->orderBy('created_at', 'asc')->orderBy('id', 'asc');
var_dump($usuario);
});
/***********************************************************************/
