<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHabitacionesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('habitaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tipo', 50);
            $table->decimal('tarifa', 9, 2);
            $table->decimal('tarifa_corporativa', 9, 2);
            $table->integer('cantidad_maxima');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('habitaciones');
    }
}
