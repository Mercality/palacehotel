<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('check_in');
            $table->date('check_out');
            $table->string('huespedes', 255);
            $table->tinyInteger('noches');
            $table->decimal('costo', 9, 2);
            $table->integer('habitacion_id')->unsigned();
            $table->integer('solicitud_id')->unsigned();
            $table->timestamps();
            $table->foreign('habitacion_id')->references('id')->on('habitaciones');
            $table->foreign('solicitud_id')->references('id')->on('solicitudes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('reservas');
    }
}
