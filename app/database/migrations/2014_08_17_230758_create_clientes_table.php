<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_rz', 30);
            $table->string('ced_rif', 15);
            $table->string('telefono', 15);
            $table->string('email', 255);
            $table->boolean('tarifa_corporativa')->default(false);
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();

        });
        Schema::table('clientes', function (Blueprint $table) {
            $table->foreign('usuario_id')->references('id')->on('usuarios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('clientes');
    }
}
