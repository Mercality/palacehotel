<?php

class Reserva extends Eloquent
{
    public function solicitud()
    {
        return $this->belongsTo('Solicitud');
    }
    public function habitacion()
    {
        return $this->belongsTo('Habitacion');
    }
    public static $rules = array(
        'habitacion_id' => array('required', 'exists:habitaciones,id'),
        'check_in' => array('required', 'date'),
        'check_out' => array('required', 'date'),
        'noches' => array('integer'),
        'costo' => array('required', 'numeric'),
        'solicitud_id' => array('required', 'exists:solicitudes,id'),
        );

    public static function validate($input)
    {
        $v = Validator::make($input, static::$rules);

        return $v->fails()
        ? $v
        : true;
    }
/*
|--------------------------------------------------------------------------
| Crear una nueva solicitud
|--------------------------------------------------------------------------
| 
| Crea una nueva instancia de la Clase Solicitud
| @return App/model/Solicitud
|
*/
    public static function crearSolicitud()
    {
        $solicitante = Input::get('solicitante');
        $solicitud = new Solicitud();
        $solicitud->solicitante = $solicitante;

        return $solicitud;
    }

/*
|--------------------------------------------------------------------------
| Crea un nuevo Cliente si no existía en la base de datos
|--------------------------------------------------------------------------
| 
| @return App/Models/Cliente
| 
*/
    public static function crearCliente($usuario)
    {
        
        $nuevoCliente =  array(
                'nombre_rz' => Input::get('nombre_rz'),
                'ced_rif' => trim(Input::get('tipoCedu')).trim(Input::get('cedrif')),
                'telefono' => Input::get('telefono'),
                'email' => Input::get('email'),
                'usuario_id' => $usuario->id,
                );
        $vC = Cliente::validate($nuevoCliente);
        if ($vC !== true) {

            return 'cliente invalido';

        } else {
            $cliente = Cliente::create($nuevoCliente);
        }

        return $cliente;
    }

    /*
|--------------------------------------------------------------------------
| Actualiza un cliente existente que coloco nuevos datos
|--------------------------------------------------------------------------
| 
| @return App/Models/Cliente
| 
*/
    public static function actualizarCliente($usuario, $cliente)
    {
        $nuevoCliente =  array(
                'nombre_rz' => Input::get('nombre_rz'),
                'ced_rif' => trim(Input::get('tipoCedu')).trim(Input::get('cedrif')),
                'telefono' => Input::get('telefono'),
                'email' => Input::get('email'),
                'usuario_id' => $usuario->id,
                );
        $vC = Cliente::validate($nuevoCliente);
        if ($vC !== true) {

            return 'cliente invalido';

        } else {
            $cliente->update($nuevoCliente);
        }
        

        return $cliente;
    }

/*
|--------------------------------------------------------------------------
| Crear Reserva
|--------------------------------------------------------------------------
| 
| Crea una nueva instancia de Reserva con los datos de la habitación
| @return App/Models/Reserva
| 
*/
    public static function crearReserva($huesped, $tipoHabitacion, $checkin, $checkout, $x, $solicitud)
    {
        $reserva = new self();
        $habitacion = Habitacion::find($tipoHabitacion);
        $reserva->habitacion_id = $habitacion->id;

        $fechaIn = new DateTime(str_replace('/', '-', $checkin[$x]));
        $fechaIn->setTimezone(new DateTimeZone('UTC'));
        $fechaOut = new DateTime(str_replace('/', '-', $checkout[$x]));
        $fechaOut->setTimezone(new DateTimeZone('UTC'));

        $diferencia = $fechaIn->diff($fechaOut);
        $reserva->huespedes = $huesped;
        $reserva->check_in = $fechaIn;
        $reserva->check_out = $fechaOut;
        $reserva->noches = $diferencia->days;
        if ($solicitud->cliente->tarifa_corporativa) {
            $reserva->costo = $habitacion->tarifa_corporativa/1.12 * $diferencia->days;
        }
        else {
            $reserva->costo = $habitacion->tarifa/1.12 * $diferencia->days;
        }
        
        $reserva->solicitud_id = $solicitud->id;

        return $reserva;
    }

/*
|--------------------------------------------------------------------------
| Recorrer cada habitación Solicitada
|--------------------------------------------------------------------------
| 
| Crea una instancia de Reserva por cada habitación solicitada
| @return Array [total, iva, habshtml]
| 
*/
    public static function iterarHabs($solicitud)
    {
        $habsHtml = '';
        $total = 0;
        $x = 0;
        $iva = 0;
        $checkin = Input::get('checkin');
        $checkout = Input::get('checkout');
        $huesped = Input::get('huesped');
//Por cada habitación se crea una instancia de Reserva, se valida la información y se genera el HTML para el PDF
        foreach (Input::get('tipohabitacion') as $key => $tipoHabitacion) {
            if ($huesped[$key] == '') {
                $huesped[$key] = 'No Especifica';
            }
            $reserva = static::crearReserva($huesped[$key], $tipoHabitacion, $checkin, $checkout, $x, $solicitud, $total);
            $total = $total + $reserva->costo;
            $iva = $iva + (round($reserva->costo * 12 / 100, 2));
            $arrReserv = $reserva->toArray();

//Validación de la información recibida, si falla en alguna habitación se elimina la solicitud.
            $vR = static::validate($arrReserv);
            if ($vR !== true) {
                Solicitud::destroy($solicitud->id);

                return 'Error en reservas'; //////////////////////////////// Pendiente Crear un error para cuando fallen las reservas
            } else {
                $reserva->save();
                $habitacion = Habitacion::find($tipoHabitacion);
                $habsHtml = $habsHtml.'<tr><td class="table-Data">'.$habitacion->tipo.'</td><td class="table-Data">'.$huesped[$x].'</td><td class="table-Data text-center">'.$checkin[$x].'</td><td class="table-Data text-center">'.$checkout[$x].'</td><td class="table-Data text-center">'.$reserva->noches.'</td><td colspan="2" class="table-Data text-right">'.number_format($reserva->costo, 2, ',', '.').'</td></tr>';
                $x = $x + 1;
            }
        }

        return array('total' => $total, 'iva' => $iva, 'habsHtml' => $habsHtml);
    }

/*
|--------------------------------------------------------------------------
| Crear Archivo PDF
|--------------------------------------------------------------------------
| 
| Luego de procesar cada habitación se procede a crear el archivo PDF de la reserva.
| @return $nombreAr     (Nombre del Archivo PDF creado) 
| @return stream()      (Ver el PDF online sin generar un archivo)
|
*/
    public static function crearPdf($cliente, $solicitud, $habsHtml, $time, $iva, $gtotal, $SoS)
    {
        $pdf = new mPDF();
        $data = array(
            'cliente' => $cliente,
            'solicitud' => $solicitud,
            'habsHtml' => $habsHtml,
            'time' => $time,
            'iva' => $iva,
            'gtotal' => $gtotal,
            );

        $pdf->WriteHTML(View::make('procesar')->with($data)->render(), 0);
        $footer = file_get_contents(base_path('app/views/pie.html'));
        $pdf->setHTMLFooter($footer);
        if ($solicitud->confirmada === true) {
            $pdf->SetWatermarkImage(base_path('public/images/sello-aprobado.gif'), 0.4, array(50, 50), array(30, 200));
            $pdf->showWatermarkImage = true;
        } elseif ($solicitud->confirmada === false) {
            $pdf->SetWatermarkImage(base_path('public/images/rechazado.jpg'), 0.4, array(50, 50), array(30, 200));
            $pdf->showWatermarkImage = true;
        } else {
            $pdf->showWatermarkImage = false;
        }
//Se utiliza parte de la razon social y la fecha actual para darle el nombre al archivo
        $nombreAr = str_replace(' ', '', substr($cliente->nombre_rz, 0, 8));
        $nombreAr = 'reserva'.$nombreAr.'-'.$time->format('dmYhis');
        $nombreAr = base_path('public/reservas/'.$nombreAr.'.pdf');
//Guardar Archivo PDF
        if ($SoS == 'save') {
            $pdf->output($nombreAr, 'F');

            return $nombreAr;
        }
//Ver en Linea Archivo PDF
        if ($SoS == 'stream') {
            return $pdf->output();
        }
    }

/*
|--------------------------------------------------------------------------
| Enviar Correo con Archivo PDF
|--------------------------------------------------------------------------
| 
| Se envía un correo electronico al email del cliente
| Se borra el archivo generado para que no ocupe espacio en el servidor
| $data es la información que puede accederse en la vista correspondiente"
| 
*/
    public static function procesarMail($solicitud, $tipoEmail, $asunto, $nombreAr = null, $emails = null)
    {     
        $data = array();
        $data['fecha'] = Carbon\Carbon::now();
        if ($solicitud) {
            $email = $solicitud->cliente->email;
            $data = array('solicitud' => $solicitud);
        }
        $emailOculto = $emails;
        switch($tipoEmail) {
            case 'Reserva':
                $emailBody = 'emails.reserva_recibida';
                break;
            case 'Aprobada':
                $emailBody = 'emails.reserva_aprobada';
                break;
            case 'Rechazada':
                $emailBody = 'emails.reserva_rechazada';
                break;
            case 'Tarifas':
                $emailBody = 'emails.cambio_tarifas';
                $email = 'reservaspalaceh@gmail.com';
                $habitaciones = Habitacion::all();
                $data['habitaciones'] = $habitaciones;
                break;
            case 'Tarifas_Corporativas':
                $emailBody = 'emails.cambio_tarifas_corporativas';
                $email = 'reservaspalaceh@gmail.com';
                $habitaciones = Habitacion::all();
                $data['habitaciones'] = $habitaciones;
                break;
            case 'Nueva_Reserva':
                $emailBody = 'emails.nueva_reserva';
                $email = ['ramonlv93@gmail.com', 'palacev@gmail.com'];
                $emailOculto = null;
                break;
        }
        Mail::send($emailBody, $data, function ($message) use ($email, $nombreAr, $asunto, $emailOculto) {
          //$message->from('reservaspalaceh@gmail.com', 'Palace Hotel, C.A.');
          $message->to($email);
          $message->subject($asunto);
          if ($nombreAr) {
            $message->attach($nombreAr);
          }
          if ($emailOculto) {
            $message->bcc($emailOculto);
          }
        });
        if ($nombreAr) {
            File::delete($nombreAr);
        }
    }
/*
|--------------------------------------------------------------------------
| Crea el html de habitaciones para mostrar PDF en Linea
|--------------------------------------------------------------------------
| 
| Return Total, IVA, HabsHTML
| 
*/
    public static function genHabs($solicitud)
    {
        $habsHtml = '';
        $total = 0;
        $x = 0;
        $iva = 0;
        foreach ($solicitud->reserva as $reserva) {
            $total = $total + $reserva->costo;
            $iva = $iva + (round($reserva->costo * 12 / 100, 2));

            $habsHtml = $habsHtml.'<tr><td class="table-Data">'.$reserva->habitacion->tipo.'</td><td class="table-Data">'.$reserva->huespedes.'</td><td  class="text-center table-Data">'.DateTime::createFromFormat('Y-m-d', $reserva->check_in)->format('d/m/Y').'</td><td  class="text-center table-Data">'.DateTime::createFromFormat('Y-m-d', $reserva->check_out)->format('d/m/Y').'</td><td  class="text-center table-Data">'.$reserva->noches.'</td><td  class="table-Data text-right" colspan="2">'.number_format($reserva->costo, 2, ',', '.').'</td></tr>';
            $x = $x + 1;
        }

        return array('total' => $total, 'iva' => $iva, 'habsHtml' => $habsHtml);
    }
}
