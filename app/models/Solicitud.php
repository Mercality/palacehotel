<?php

class Solicitud extends Eloquent
{
    protected $table = 'solicitudes';
    public function usuario()
    {
        return $this->belongsTo('Cliente');
    }
    public function reserva()
    {
        return $this->hasMany('Reserva');
    }
    public function cliente()
    {
        return $this->belongsTo('Cliente');
    }

    public static $rules = array(
                                'cliente_id' => array('required', 'exists:clientes,id'),
                                'solicitante' => array('required', 'min:5'),
        );
    public static function validate($input)
    {
        $v = Validator::make($input, static::$rules);

        return $v->fails()
                ? $v
                : true;
    }
    public function numeroSolicitud()
    {
        return sprintf('%06d', $this->id);
    }
}
