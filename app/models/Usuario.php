<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use Illuminate\Auth\Reminders\RemindableTrait;

class Usuario extends Eloquent implements UserInterface, RemindableInterface
{
    use RemindableTrait;
    public function cliente()
    {
        return $this->hasOne('Cliente');
    }
    public static $rules = array(
                                'identificacion' => array('required', 'min:6', 'max:20','regex:/^[a-zA-Z0-9]+$/', 'unique:usuarios,identificacion'),
                                'password' => array('required', 'min:8', 'max:20'),
                                'tipo' => 'min:3',
                                'email' => array('required', 'email', 'unique:usuarios,email'),
        );
    public static function rulesEdit($id)
    {
        $rulesEdit = array(
                                'identificacion' => array('required', 'min:6', 'max:20','regex:/^[a-zA-Z0-9]+$/', 'unique:usuarios,identificacion,'.$id),
                                'tipo' => 'min:3',
                                'email' => array('required', 'email', 'unique:usuarios,email,'.$id),
        );

        return $rulesEdit;
    }
    public static function rulesPassword()
    {
        $rulesEdit = array(
                                'password' => array('required', 'min:8', 'max:20'),
        );

        return $rulesEdit;
    }
    public static function validatePassword($input)
    {
        $v = Validator::make($input, static::rulesPassword());

        return $v->fails()
                ? $v
                : true;
    }
    public static function validate($input)
    {
        $v = Validator::make($input, static::$rules);

        return $v->fails()
                ? $v
                : true;
    }
    public static function validateEdit($input, $id)
    {
        $v = Validator::make($input, static::rulesEdit($id));

        return $v->fails()
                ? $v
                : true;
    }
    protected $fillable = array('identificacion', 'password', 'tipo', 'email');

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getReminderEmail()
    {
        return $this->email;
    }
}
