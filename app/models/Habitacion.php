<?php

class Habitacion extends Eloquent
{
    public function reserva()
    {
        return $this->hasMany('Reserva');
    }
    public static $rules = array(
                                'tarifa' => array('required', 'min:4', 'max:30','regex:/[\d]{2},[\d]{2}/'),
        );
    public static function validate($input)
    {
        $v = Validator::make($input, static::$rules);

        return $v->fails()
                ? $v
                : true;
    }
    protected $guarded = array();
    protected $table = 'habitaciones';
}
