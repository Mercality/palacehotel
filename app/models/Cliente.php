<?php

class Cliente extends Eloquent
{
    public function usuario()
    {
        return $this->belongsTo('Usuario');
    }
    public function solicitud()
    {
        return $this->hasMany('Solicitud');
    }
    public static $rules = array(
                                'nombre_rz' => array('required', 'min:6', 'max:30','regex:/^[a-zA-Z0-9_. ,]+$/'),
                                'ced_rif' => array('required', 'min:6', 'max:15'),
                                'telefono' => array('min:6','max:100', 'regex:/\b\d{4}\-\d{3}\-\d{4}$/'),
                                'usuario_id' => array('required', 'exists:usuarios,id'),
                                'email' => array('required', 'email'),
        );
    public static function validate($input)
    {
        $v = Validator::make($input, static::$rules);

        return $v->fails()
                ? $v
                : true;
    }
    public function getTipoCedu()
    {
        return substr($this->ced_rif, 0, 1);
    }
    public  function getNumericCed()
    {
        return substr($this->ced_rif, 1, 10);
    }

    protected $fillable = array('nombre_rz', 'ced_rif', 'telefono', 'usuario_id', 'email');
}
