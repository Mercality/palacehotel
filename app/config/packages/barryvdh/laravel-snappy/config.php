<?php

return array(

    'pdf' => array(
        'enabled' => true,
        'binary' => base_path('app/libraries/wkhtmltopdf-linux-amd64'),
        'options' => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary' => '/usr/local/bin/wkhtmltoimage',
        'options' => array(),
    ),

);
