<section class="row">
	<div class="col-md-6 presentacion">
		<legend><h1 id="tascares" class="text-center">Tasca-Restaurant</h1></legend>
							<!--CAROUSEL-->
						<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
							    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
							    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
							    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
							  </ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
							    <div class="item active">
							      <img src="{{asset('images/rest/DSC_2365.jpg')}}" alt="...">
							      <div class="carousel-caption">
							        Tasca-Restaurant
							      </div>
							    </div>
							    <div class="item">
							      <img src="{{asset('images/rest/rest2.jpg')}}" alt="...">
							      <div class="carousel-caption">
							       	Tasca-Restaurant
							      </div>
							    </div>

							    <div class="item">
							      <img src="{{asset('images/rest/DSC_2363.jpg')}}" alt="...">
							      <div class="carousel-caption">
							        Tasca-Restaurant
							      </div>
							    </div>
							    
							</div>

							  <!-- Controls -->
							  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a><!--Fin Controls -->
						</div><!--FIN CAROUSEL-->
	</div>

	<div class="col-md-6 presentacion">
		<legend><h1 id="tascares" class="text-center">Salón de festejos</h1></legend>
							<!--CAROUSEL-->
						<div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
							  <!-- Indicators -->
							  <ol class="carousel-indicators">
							    <li data-target="#carousel-example-generic2" data-slide-to="0" class="active"></li>
							    <li data-target="#carousel-example-generic2" data-slide-to="1"></li>
							    <li data-target="#carousel-example-generic2" data-slide-to="2"></li>
							  </ol>

							<!-- Wrapper for slides -->
							<div class="carousel-inner" role="listbox">
							    <div class="item active">
							      <img src="{{asset('images/rest/a3.jpg')}}" alt="...">
							      <div class="carousel-caption">
							        Salón de festejos
							      </div>
							    </div>
							    <div class="item">
							      <img src="{{asset('images/rest/a2.jpg')}}" alt="...">
							      <div class="carousel-caption">
							       	Salón de festejos
							      </div>
							    </div>

							    <div class="item">
							      <img src="{{asset('images/rest/a1.jpg')}}" alt="...">
							      <div class="carousel-caption">
							        Salón de festejos
							      </div>
							    </div>
							    
							</div>

							  <!-- Controls -->
							  <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
							    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
							    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a><!--Fin Controls -->
						</div><!--FIN CAROUSEL-->
	</div>
</section>

<div id="solicitarres"><a href="#" class="btn btn-block btn-default">Solicite su presupuesto</a></div>