<div id="qs">
	<div class="container">
	<div class="white">
		<h1>Quiénes Somos</h1>

		<div id="quienesSomos" class="row">
			
			<div class="col-md-5">
				<article>
					<h3>Bienvenidos a Palace Hotel, C.A.</h3>
					<p>Moderno y confortable, situado en el centro de Valle de la Pascua a minutos de Bancos y Centros Comerciales. Un grato ambiente familiar donde cuidamos todos los detalles para que la estancia de nuestros huéspedes sea una experiencia grata. Palace Hotel, C.A. y usted hacen la combinación perfecta para un viaje placentero.
					</p>

					<p>“Palace Hotel, C.A. le brinda las mejores posibilidades en servicio, haciendo su estadía agradable, para que se sienta totalmente a gusto ...”
					</p>

					<p>Cuando piense en viajar a Valle de la Pascua en plan de negocios, descanso o vivir, Palace Hotel, C.A. lo espera.
					</p>
				</article>
			</div>
			
			<div class="col-md-6 col-md-offset-1">
				<article>
					<h3>Valle de la Pascua</h3>
					<p>Valle de la Pascua es la ciudad capital del municipio autónomo Leonardo Infante, en el estado Guárico, Venezuela. La ciudad se encuentra en los llanos centrales de Venezuela y fue fundada el 25 de febrero de 1785 por el padre Mariano Martí.
					</p>
				</article>
				<article>
					<h3>Motivos para visitar Valle de la Pascua</h3>
					<div class="row">
						<div class="col-md-6">
							<ul>
								<li>Ferias de la Candelaria</li>
								<li>Exposición Nacional Canina</li>
								<li>Parque Recreacional “El Corozo”</li>
								<li>Festival “Panoja de Oro”</li>
								<li>Comidas Criollas</li>
								<li>Campeonato Nacional de Coleo</li>
							</ul>
						</div>
						<div class="col-md-6">
							<img src="{{asset('images/img-corrida.jpg')}}" alt="" class="img-circle">
						</div>
					</div>
				</article>
			</div>

		</div>
	</div><!--white-->
	</div>
</div>