<div id="pre-habitaciones">
	<div class="container">
		<div class="row">
			<h1>Habitaciones</h1>
			<div class="col-md-8 col-md-offset-2">
				<article>
					<p>Palace Hotel, C.A. cuenta con diferentes tipos de habitaciones que se ajustan a su presupuesto y necesidad. Le ofrecemos 80 habitaciones con balcón distribuídas en cincos (5) pisos, diseñadas bajo las últimas tendencias arquitectónicas y la más alta tecnología; acompañadas de una variada gama de servicios, seguridad y confort que harán que usted se sienta como en su casa.</p>
				</article>
			</div>
		</div>

		<div class="row">
			<h1>Tipos de habitaciones</h1>
			<div class="col-sm-6 col-md-4 col-md-offset-2">
				<ul>
					<li>Matrimonial</li>
					<li>Doble (Matrimonial + 1 Ind.)</li>
					<li>Triple (3 Camas Ind.)</li>
					<li>Cuádruple II (2 Mat.)</li>
					<li>Cuádruple III (1 Mat + 2 Ind.)</li>
				</ul>
			</div>
			<div class="col-sm-6 col-md-4">
				<ul>
					<li>Cuádruple IV (4 Camas Ind.)</li>
					<li>Junior Ejecutiva</li>
					<li>Palace Ejecutiva</li>
					<li>Palace Familiar</li>
					<li><a href="/habitaciones">Ver más...</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>