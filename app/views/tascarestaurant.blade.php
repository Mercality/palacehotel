<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/favicon.png" />
  <title>Palace Hotel, C.A. - tasca Restaurant</title>
  {{ HTML::style('css/bootstrap.css'); }}
  {{ HTML::style('css/styles.css'); }}
</head>
<body id="tasca">
  @include('header') <!-- Header -->
  
  <div class="container">
    <div id="tasca-restaurant" class="content"> 
    
       @include('snippets.snippet_multi_carousel_rest')

    </div> <!-- Content -->
  </div> <!-- Container -->
  @include('footer')
  
  {{ HTML::script('js/jquery.js'); }}
  {{ HTML::script('js/myscripts.js'); }}
  {{ HTML::script('js/bootstrap.js'); }}
</body>
</html>
