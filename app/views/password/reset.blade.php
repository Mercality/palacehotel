<form action="{{ action('RemindersController@postReset') }}" method="POST">
    <input type="hidden" name="token" value="{{ $token }}">
    <label for="email">Dirección de Correo Electrónico: </label><input type="email" name="email"> <br>
    <label for="password">Nueva Contraseña: </label><input type="password" name="password"><br>
    <label for="password_confirmation">Confirme la nueva contraseña: </label><input type="password" name="password_confirmation"><br>
    <input type="submit" value="Reestablecer Contraseña">
</form>