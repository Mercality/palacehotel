<!DOCTYPE html>
<?php
setlocale(LC_ALL, 'es_VE.utf8');
?>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title></title>
<style>
	@page { sheet-size: letter; }
	.text-right {
		text-align: right;
	}
	.text-center {
		text-align: center;
	}
	.subtitulo {
		width: 2%;
	}
	th {
		background-color: lightgrey;
	}

	table {
		border-collapse: collapse;
		width: 100%;

	}
	table, td, th {
		border: 1px solid darkgrey;
	}
	.tabla-Datos {
		font-size: 12px;
	}

	td, th, tr {
		padding: 2px;
	}
	.totales {
		margin-top:10px;
		font-size: 12px;
	}
	.tituloReserva {
		text-align: center;
		margin-top: 3px;
	}
	.Gerente {
		text-align: center;


	}
	.Gerente p {
		padding: 0;
		margin: 0;
	}
	.heading {
		border: 0px;
	}
	.vacio {
		width: 66%;
		border: 0px;
	}
	.noElim {
		width: 16%;
	}
	.tipohab {
		width:32%;
	}
	.huespedes {
		width: 30%;
	}

	.table-Data {
		font-size: 10px;
	}
	#sello { position: absolute; bottom: 0px; right: 0px; width: 180px; height: 180px;  }
</style>
</head>
<body id="rProcesar">
	<div class="">
		<div>
			<table class="heading" >
				<colgroup>
				<col span="1" id="col134">
				<tr style="border:none; padding:0px;">
					<td style="border:none; padding:0px;">
						<img  src="{{ base_path('/public/images/logo_reporte.jpg') }}" alt="Logo">
					</td>
					<td style="border:none; padding:0px;">
						<h1 style="font-family: Times;">PALACE HOTEL, C.A.</h1>
						<p id="direcc">Av. Rómulo Gallegos c/c calle Shettino<br>
							Telefax: 0235-3422073 / 3422259 / 3422440 / 3422339<br>
							Valle de la Pascua - Edo. Guárico<br>
							RIF Nº J-30524194-5<br>
						</p>
					</td>
				</tr>
			</table>
		</div>
		<h4 class="text-right">Valle de la Pascua {{ utf8_encode(strftime("%d de %B de %Y",$time->getTimeStamp())); }}</h4><br>
		<h3 class="text-right">Solicitud N°: <strong>{{$solicitud->numeroSolicitud()}}</strong></h3>
		<h2 class="tituloReserva">Solicitud de Reserva y Presupuesto</h2>
		<table class="table table-bordered tabla-Datos">

		<tbody >
			<tr><th colspan="7" class="well">Datos del Solicitante</th></tr>
			<tr>
				<td class="subtitulo"><strong>Nombre/Razón Social</strong></td>
				<td colspan="2" >{{ e($cliente->nombre_rz) }}</td>
				<td colspan="4"><strong>Comentarios</strong></td>
			</tr>
			<tr>
				<td class="subtitulo"> <strong>Cédula / RIF</strong></td>
				<td colspan="2" >{{ e($cliente->ced_rif) }}</td>
				<td rowspan="4" colspan="4" id="comenta">{{e($solicitud->comentarios)}}</td>
			</tr>
			<tr>
				<td class="subtitulo"><strong>Solicitante</strong></td>
				<td colspan="2" >{{ e($solicitud->solicitante) }}</td>
			</tr>
			<tr>
				<td class="subtitulo"><strong>E-Mail</strong></td>
				<td colspan="2" >{{ e($cliente->email) }}</td>
			</tr>
			<tr>
				<td class="subtitulo"><strong>Teléfono</strong></td>
				<td colspan="2" >{{ e($cliente->telefono) }}</td>
			</tr>
			<tr><th colspan="7" class="well">Habitaciones</th></tr>
			<tr >
				<td class="tipohab"><strong>Tipo de habitación</strong></td>
				<td class="huespedes"><strong>Huesped(es)</strong></td>
				<td class="text-center"><strong>Check-In</strong></td>
				<td class="text-center"><strong>Check-Out</strong></td>
				<td class="text-center"><strong>Noches</strong></td>
				<td class="text-center" colspan="2"><strong>Subtotal</strong></td>
			</tr>
			{{ $habsHtml }}
		</tbody>
	</table>
	<div class="tablaTotales">
		<table class="totales" style="border:0px;">
			<col span="1" id="col7">
			<col span="1" id="col8">
			<tbody>
				<tr >
					<td class="vacio"></td>
					<td class="noElim">Base Imponible(G)</td>
					<td class=" text-right">{{ e(number_format($solicitud->costo_total,2,',','.')) }}</td>
				</tr>
				<tr >
					<td class="vacio"></td>
					<td class="noElim">I.V.A.(12%)</td>
					<td class=" text-right">{{ number_format($iva,2,',','.') }}</td>
				</tr>
				<tr >
					<td class="vacio"></td>
					<td class="noElim">Total a Pagar(Bs)</td>
					<td class=" text-right">{{ number_format($gtotal,2,',','.') }}</td>
				</tr>
			</tbody>
		</table>
	</div>
	<br><br><br><br><br>
	<p style="text-decoration:underline">(No se garantiza la reserva hasta recibir comprobante de pago. Presupuesto válido por 48 horas. Tarifas sujetas a cambio sin previo aviso)</p>
	<br><br>
	<div class="text-center Gerente">

		<p >Lcda. María Beatríz De Freitas de Quintal</p>
		<p>Gerente</p>
	</div>
</div>
</body>
</html>
