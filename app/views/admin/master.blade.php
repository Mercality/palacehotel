<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Palace Hotel, C.A. - Panel Administrativo</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('/css/adminstyles.css')}}" rel="stylesheet">
    <link href="{{asset('/css/styles.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('/css/sb-admin.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset('/css/plugins/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
@if(Session::get('errores'))
      <div class="modal fade" id="smallModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
              <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
              <p>
                Hubo un error al actualizar el precio de la habitacion: <strong>@foreach(Session::get('habitacion') as $id => $tipo) {{$tipo}}, @endforeach</strong> asegurese que el formato utilizado es de numeros separados por comas sin usar separador de miles. Ejemplo: 1000,01
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
   @endif
   <!-- MODAL CONFIRMACION DE APROBAR O RECHAZAR -->

   <div class="modal fade" id="smallModal2">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
              <h4 class="modal-title" id="tituloConfirmacion">Confirmacion de Reserva</h4>
            </div>
            <div class="modal-body">
              <p id="textoConfirmacion">
                
              </p>
            </div>
            <div class="modal-footer">
              <a id="bSi"><button type="button" class="btn btn-default">Si</button></a>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- MODAL CONFIRMACION DE ACEPTAR O ELIMINAR -->

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/admin')}}">Palace Hotel, C.A.</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Ramon Ledezma</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Hoy</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Ramon Ledezma <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Mensajes</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracion</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Cerrar Sesion</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li >
                        <a href="{{url('/admin')}}"><i class="fa fa-fw fa-dashboard"></i> Panel Administrativo</a>
                    </li>
                    <li >
                        <a href="{{url('/admin/usuarios')}}"><i class="fa fa-fw fa-user"></i> Usuarios</a>
                    </li>
                    <li >
                        <a href="{{url('/admin/reservas?ver=nuevas')}}"><i class="fa fa-fw fa-file"></i> Reservas</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/estadisticas')}}"><i class="fa fa-fw fa-bar-chart-o"></i> Estadisticas</a>
                    </li>
                    <li class="active">
                        <a href="{{url('/admin/habitaciones')}}"><i class="glyphicon glyphicon-home"></i> Habitaciones</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Panel Administrativo <small>Habitaciones</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> <a href="{{url('admin')}}">Panel Administrativo</a>
                            </li>
                            <li class="active">
                                <i class="glyphicon glyphicon-home"></i> Habitaciones
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-home"></i> Habitaciones</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped">
                                      <form action="{{ url('admin/habitaciones') }}" method="POST">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID(#)</th>
                                                <th class="text-center">Tipo Habitacion</th>
                                                <th class="text-center">Tarifa</th>
                                                <th class="text-center">Tarifa Corp.</th>
                                                <th class="text-center">Nueva Tarifa(Base)</th>
                                                <th class="text-center">Nueva T. Corp.(Base)</th>
                                                <th class="text-center">Ultima Actualizacion</th>
                                            </tr>
                                        </thead>
                                        <tbody  class="bodyTarifas">
                                        @foreach($habitaciones as $habitacion)
                                            <tr>
                                                <td class="text-center">{{ e($habitacion->id) }}</td>
                                                <td>{{ e($habitacion->tipo) }}</td>
                                                <td class="text-right">{{ number_format(e($habitacion->tarifa),2,',','.') }}</td>
                                                <td class="text-right">{{ number_format(($habitacion->tarifa_corporativa),2,',','.') }}</td>
                                                <td><input size="3" type="text" class="form-control tarifas" name="nuevaTarifa[{{$habitacion->id}}]"></td>
                                                <td><input size="3" type="text" class="form-control tarifas" name="nuevaTarifaC[{{$habitacion->id}}]"></td>
                                                <td class="text-center">{{ e($habitacion->updated_at->format('d/m/Y h:i:s A')) }}</td>
                                            </tr>
                                        @endforeach
                                        <tr><td colspan="7"><button class="btn btn-primary btn-block" type="submit">Actualizar</button></td></tr>
                                        
                                        </tbody>
                                      </form>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="{{asset('js/jquery.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{asset('/js/plugins/morris/raphael.min.js')}}"></script>
    <script src="{{asset('/js/plugins/morris/morris.min.js')}}"></script>
    <script src="{{asset('/js/plugins/morris/morris-data.js')}}"></script>
    <script src="{{asset('/js/myscripts.js')}}"></script>
    <script src="{{asset('/js/adminscripts.js')}}"></script>

</body>

</html>