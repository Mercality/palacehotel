<table class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th>IP</th>
			<th>Pagina</th>
			<th>Usuario</th>
			<th>Fecha</th>
			<th>Navegador</th>
		</tr>
	</thead>

	<tbody>
		@foreach($visitors as $visitor)
			<tr>
				<td>{{ $visitor->ip }}</td>
				<td>{{ $visitor->page }}</td>
				@if($visitor->isUser())
					<td>{{ Usuario::whereId($visitor->user)->first()->identificacion }}</td>
				@else
					<td>Guest</td>
				@endif
				<td>{{ $visitor->updated_at->format('d/m/Y h:i:s A') }}</td>
				<td>{{ $visitor->platform }} -> {{ $visitor->browser }}</td>
			</tr>
		@endforeach
	</tbody>

</table>
                   {{ $visitors->links() }}