<!DOCTYPE html>
<html lang="es">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Palace Hotel, C.A. - Panel Administrativo</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('/css/adminstyles.css')}}" rel="stylesheet">
    <link href="{{asset('/css/styles.css')}}" rel="stylesheet">


    <!-- Custom CSS -->
    <link href="{{asset('/css/sb-admin.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset('/css/plugins/morris.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body>
    @if(Session::get('confirmada'))
    <div class="modal fade" id="smallModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title">Confirmacion de Reserva</h4>
                </div>
                <div class="modal-body">
                    <p>
                        @if(Session::get('confirmada') == 'si')
                        {{ 'La reserva bajo el numero: '.Session::get('id').' ha sido confirmada' }}
                        @endif
                        @if(Session::get('confirmada') == 'no')
                        {{ 'La reserva bajo el numero: '.Session::get('id').' ha sido rechazada' }}
                        @endif
                        @if(Session::get('confirmada') == 'error')
                        {{ 'Hubo un error al procesar su solicitud' }}
                        @endif
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endif
    <!-- MODAL CONFIRMACION DE APROBAR O RECHAZAR -->

    <div class="modal fade" id="smallModal2">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
                    <h4 class="modal-title" id="tituloConfirmacion">Confirmacion de Reserva</h4>
                </div>
                <div class="modal-body">
                    <p id="textoConfirmacion">

                    </p>
                </div>
                <div class="modal-footer">
                    <a id="bSi"><button type="button" class="btn btn-default">Si</button></a>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- MODAL CONFIRMACION DE ACEPTAR O ELIMINAR -->

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/admin')}}">Palace Hotel, C.A.</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>Ramon Ledezma</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Hoy</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Ramon Ledezma <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Perfil</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Mensajes</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Configuracion</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-power-off"></i> Cerrar Sesion</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li >
                        <a href="{{url('/admin')}}"><i class="fa fa-fw fa-dashboard"></i> Panel Administrativo</a>
                    </li>
                    <li >
                        <a href="{{url('/admin/usuarios')}}"><i class="fa fa-fw fa-user"></i> Usuarios</a>
                    </li>
                    <li class="active">
                        <a href="{{url('/admin/reservas?ver=nuevas')}}"><i class="fa fa-fw fa-file"></i> Reservas</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/estadisticas')}}"><i class="fa fa-fw fa-bar-chart-o"></i> Estadisticas</a>
                    </li>
                    <li>
                        <a href="{{url('/admin/habitaciones')}}"><i class="glyphicon glyphicon-home"></i> Habitaciones</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Dropdown <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                            <li>
                                <a href="#">Dropdown Item</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Panel Administrativo <small>Reservas</small>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i> <a href="{{url('admin')}}">Panel Administrativo</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Reservas
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="glyphicon glyphicon-file fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $nuevasReservas }}</div>
                                        <div>Nuevas Reservas</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{url('admin/reservas?ver=nuevas')}}">
                                <div class="panel-footer @if(Input::get('ver') == 'nuevas') {{'activo'}} @endif">
                                    <span class="pull-left">Ver Nuevas Reservas</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-green">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="glyphicon glyphicon-file fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $confirmadas }}</div>
                                        <div>Reservas Aprobadas</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{url('admin/reservas?ver=aprobadas')}}">
                                <div class="panel-footer @if(Input::get('ver') == 'aprobadas') {{'activo'}} @endif">
                                    <span class="pull-left ">Ver Reservas Aprobadas</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-file fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $rechazadas }}</div>
                                        <div>Reservas Rechazadas</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{url('admin/reservas?ver=rechazadas')}}">
                                <div class="panel-footer @if(Input::get('ver') == 'rechazadas') {{'activo'}} @endif">
                                    <span class="pull-left">Ver Reservas Rechazadas</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>    
                    <!-- /.row -->

                    <div class=" active col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="glyphicon glyphicon-file fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">{{ $todas }}</div>
                                        <div>Todas las Reservas</div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{url('admin/reservas?ver=todas')}}">
                                <div class="panel-footer @if(Input::get('ver') == 'todas') {{'activo'}} @endif">
                                    <span class="pull-left">Ver Todas las Reservas</span>
                                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-file fa-fw"></i> Reservas</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table  table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID(#)</th>
                                                <th class="text-center">Solicitante</th>
                                                <th class="text-center">Fecha Solicitud</th>
                                                <th class="text-center">Tipo</th>
                                                <th class="text-center">Check-IN</th>
                                                <th class="text-center">Check-OUT</th>
                                                <th class="text-center">Noches</th>
                                                <th class="text-center">Costo</th>
                                                <th class="text-center">Total Reserva</th>
                                                <th class="text-center">Ver PDF</th>
                                                <th colspan="2" class="text-center">Aprobar / Rechazar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($solicitudes as $solicitud)
                                            @foreach($solicitud->reserva as $key => $reserva)
                                            @if($key == 0)
                                            <tr class="@if($solicitud->confirmada) {{'aprobado'}} @elseif($solicitud->confirmada === 0) {{'rechazado'}}  @else {{'noRevisada'}} @endif">
                                                <td class="text-center" rowspan="{{$solicitud->reserva->count()}}">{{$solicitud->numeroSolicitud()}}</td>
                                                <td rowspan="{{$solicitud->reserva->count()}}">{{$solicitud->cliente->nombre_rz}}</td>
                                                <td class="consolas" rowspan="{{$solicitud->reserva->count()}}">{{$solicitud->created_at->format('d/m/Y H:i:s')}}</td>
                                                <td>{{$reserva->habitacion->tipo}}</td>
                                                <td class="consolas">{{ DateTime::createFromFormat('Y-m-d', $reserva->check_in)->format('d/m/Y')}}</td>
                                                <td class="consolas">{{DateTime::createFromFormat('Y-m-d', $reserva->check_out)->format('d/m/Y')}}</td>
                                                <td class="consolas">{{$reserva->noches}}</td>
                                                <td class="montos text-right">{{$reserva->costo}}</td>
                                                <td class="montos text-right" rowspan="{{$solicitud->reserva->count()}}">{{$solicitud->costo_total}}</td>
                                                <td class="botonPDF" rowspan="{{$solicitud->reserva->count()}}" class="text-center"><a target="_blank" href="{{url('admin/reservas/mostrarR/'.$solicitud->id)}}"><span class="glyphicon glyphicon-file"></span></a></td>
                                                <td class="botonAprobar" rowspan="{{$solicitud->reserva->count()}}" class="text-center"><a class="aprobar" href="{{url('admin/reservas/confirmar/'.$solicitud->id.'?conf=si')}}"><span class="glyphicon glyphicon-ok"></span></a></td>
                                                <td class="botonRechazar" rowspan="{{$solicitud->reserva->count()}}" class="text-center"><a class="rechazar" href="{{url('admin/reservas/confirmar/'.$solicitud->id.'?conf=no')}}"><span class="glyphicon glyphicon-remove"></span></a></td>
                                            </tr>
                                            @else
                                            <tr class="@if($solicitud->confirmada) {{'aprobado'}} @elseif($solicitud->confirmada === 0) {{'rechazado'}} @else {{'noRevisada'}} @endif">
                                                <td>{{$reserva->habitacion->tipo}}</td>
                                                <td class="consolas">{{DateTime::createFromFormat('Y-m-d', $reserva->check_in)->format('d/m/Y')}}</td>
                                                <td class="consolas">{{DateTime::createFromFormat('Y-m-d', $reserva->check_out)->format('d/m/Y')}}</td>
                                                <td class="consolas">{{$reserva->noches}}</td>
                                                <td class="montos text-right">{{$reserva->costo}}</td>
                                            </tr>
                                            @endif
                                            @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <?php echo $solicitudes->links(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery Version 1.11.0 -->
    <script src="{{asset('js/jquery.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{asset('js/bootstrap.js')}}"></script>

    <!-- Morris Charts JavaScript -->
   <!-- <script src="{{asset('/js/plugins/morris/raphael.min.js')}}"></script>
    <script src="{{asset('/js/plugins/morris/morris.min.js')}}"></script>
    <script src="{{asset('/js/plugins/morris/morris-data.js')}}"></script> -->

    <script src="{{asset('/js/myscripts.js')}}"></script>
    <script src="{{asset('/js/adminscripts.js')}}"></script>
    <script type="text/javascript">
    $(function () {
  $('.tooltp').tooltip({delay: -5});
})
    </script>


</body>

</html>