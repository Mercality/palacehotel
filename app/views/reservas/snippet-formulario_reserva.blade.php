<section class="formreserva col-md-8 col-md-offset-2">

    <legend><h2>Solicitud de Reserva</h2></legend>
    <div id="loading" class="text-center">
      <img src="{{ asset('/images/loading.gif') }}" alt="Loading">
    </div>
    @if(!$s)
      <form action="/reserva/login" id="formLogin" method="post" class="form-horizontal hidden">
        <div class="form-group">
          <label for="solicitante" class="col-xs-4 control-label">¿Es usuario Registrado?</label>
          <div class="form-inline col-xs-8">
            <input type="radio" class="" name="registrado" id="siRegistrado" value="1">Si&nbsp
            <input type="radio" checked class="" name="registrado" id="noRegistrado" value="0">No&nbsp&nbsp
            <span id="ocultoRes" class="ocultoRes">Usuario:&nbsp<input type="text" name="identificacion" size="8" class="form-control">&nbspClave:&nbsp<input type="password" name="password" size="8" class="form-control">
            <button type="submit" class="btn btn-default" id="loginBut">Login</button><a title="¿Olvido su Contraseña?" href="/password"><span style="font-size:1.7em; vertical-align:middle; color:#840404" class="glyphicon glyphicon-question-sign"></span></a>
            </span>
            
            <a href="usuario/registro"><button type="button" class="btn btn-default " id="registroBut">Registro</button></a>
          </div>
        </div> <!-- Usuario Registrado -->
      </form>
    @endif 
    @if($s)
      <div class="form-horizontal">
        <div class="form-group">
          <div class="col-xs-4 control-label"><strong>Bienvenido</strong></div>
          <div class="form-inline col-xs-8">
            <span><strong>Sesion iniciada como: {{ Auth::user()->identificacion }}</strong></span>
            <a href="/logout"><button type="button" class="btn btn-default" id="t">Cerrar Sesión</button></a>
            <a href="usuario/editar"><button type="button" class="btn btn-default " id="editarBut">Editar</button></a>
            @if(Auth::user()->tipo == 'admin')
            <a href="/admin"><span class="boton-admin glyphicon glyphicon-cog"></span></a>
            @endif
          </div>
        </div>
    </div>
    @endif


  <form id="formReserva" class="form-horizontal hidden" method="post" role="form" action="/reserva/reservar"><fieldset id="camposForm">

    <div class="modal fade bs-example-modal-lg" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Volver</span></button>
            <h4 class="modal-title">Solicitud de Presupuesto</h4>
          </div>
          <div class="modal-body">
            <table class="table table-bordered">
              <colgroup>
              <col span="1" style="width: 25%;">
              <col span="1" style="width: 29%;">
              <col span="1" style="width: 12%;">
              <col span="1" style="width: 12%;">
              <col span="1" style="width: 5%;">
              <col span="1" style="width: 17%;">
            </colgroup>
              <tbody id="tablaConfRes">
              <tr><th colspan="7" class="well">Datos del Solicitante</th></tr>
              <tr>
                <td><strong>Nombre/Razón Social</strong></td>
                <td colspan="2" id="nrz"></td>
                <td colspan="3"><strong>Comentarios</strong></td>
                
              </tr>
              <tr class="hidden">
                <td><strong>Solicitante</strong></td>
                <td colspan="2" id="sol"></td>
              </tr>
              <tr>
                <td><strong>E-Mail</strong></td>
                <td colspan="2" id="ema"></td>
                <td rowspan="3" colspan="3" id="comenta"></td>
              </tr>
              <tr>
                <td><strong>Teléfono</strong></td>
                <td colspan="2" id="tel"></td>
              </tr>
              <tr>
                <td> <strong>Cédula / RIF</strong></td>
                <td colspan="2" id="cr"></td>
              </tr>
              <tr><th colspan="7" class="well">Habitaciones</th></tr>
              <tr id="lish">
                <td><strong>Tipo de habitación</strong></td>
                <td><strong>Huésped(es)</strong></td>
                <td><strong>Check-In</strong></td>
                <td><strong>Check-Out</strong></td>
                <td><strong>Noches</strong></td>
                <td><strong>Subtotal</strong></td>
              </tr>
              </tbody>
            </table>
            <table class="table table-bordered" style="width:36.5%; position:relative; left:63.5%;">
            <col span="1" style="width: 54%;"></col>
              <col span="1" style="width: 46%; font-family:consolas;"></col>
                <tbody>
                  <tr id="baseI">
                    <td class="noElim">Base Imponible(G)</td>             
                  </tr>
                  <tr id="iva">
                   <td class="noElim">I.V.A.(12%)</td>      
                  </tr>
                  <tr id="totalPagar">
                    <td class="noElim">Total a Pagar</td>      
                  </tr>
                </tbody>
              </table>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Volver</button>
            <button type="submit" class="btn btn-default" id="submitBut">Confirmar</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <div class="form-group">
      <label for="nombre_rz" class="col-xs-4 col-sm-4 control-label">Nombre/Razón Social</label>
      <div class="col-xs-8 col-sm-8">
        <input type="text" class="form-control" value="@if(isset($cliente)){{$cliente['nombre_rz']}}@endif" name="nombre_rz" id="nombre_rz" placeholder="Ej: Palace Hotel, C.A." @if($s) {{'disabled'}} @endif>
      </div>
    </div> <!-- Nombre/Razon Social -->

    <div class="form-group">
      <label for="cedrif" class="col-xs-4 col-sm-4 control-label">Cédula / R.I.F.</label>
      <div class="col-xs-8 col-sm-8">
        <div class="input-group">
          <div class="input-group-btn">
            <button type="button" id="tipoCed" class="btn btn-default 
            dropdown-toggle  @if($s) {{'disabled'}} @endif" data-toggle="dropdown">@if(isset($cliente)) {{substr($cliente['ced_rif'],0,1)}}  @else V @endif<span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
           <li><a id="Ven">V</a></li>
           <li><a id="Ex">E</a></li>
           <li><a id="Jud">J</a></li>
           <li><a id="Gob">G</a></li>
           <li><a id="Pas">P</a></li>

         </ul>
       </div><!-- /btn-group -->
       <input type="hidden" value="@if(isset($cliente)){{substr($cliente['ced_rif'],0,1)}}@else V @endif" id="tipoCedu" name="tipoCedu">
       <input type="text" value="@if(isset($cliente)){{str_replace(array('V','E','P','G','J'),'',$cliente['ced_rif'])}}@endif" class="form-control" name="cedrif" id="cedrif" placeholder="Ej: 305241945" @if($s) {{'disabled'}} @endif>
     </div>
   </div>
 </div> <!-- Indetificacion -->

    <div class="form-group">
      <label for="email" class="col-xs-4 col-sm-4 control-label">E-mail</label>
      <div class="col-xs-8 col-sm-8">
        <input type="text" class="form-control" value="@if(isset($cliente)){{$cliente['email']}}@endif" name="email" id="email" placeholder="Ej: huesped@gmail.com" @if($s) {{'disabled'}} @endif>  
      </div>
    </div> <!-- Email -->

    <div class="form-group">
      <label for="telefono" class="col-xs-4 col-sm-4 control-label">Teléfono</label>
      <div class="col-xs-8 col-sm-8">
        <input type="text" class="form-control" name="telefono" value="@if(isset($cliente)){{$cliente['telefono']}}@endif" id="telefono" placeholder="Ej: 04245555555" @if($s) {{'disabled'}} @endif>
      </div>
    </div> <!-- Telefono -->

    <div class="form-group">
      <label for="solicitante" class="col-xs-4 col-sm-4 control-label">Solicitante</label>
      <div class="col-xs-8 col-sm-8">
        <input type="text" class="form-control" name="solicitante" id="solicitante" placeholder="Ej: José Ramírez">
      </div>
    </div> <!-- Solicitante -->




  <div class="form-group habitas">
    <span class="form-horizontal col-sm-6 col-md-6">
      <label for="tipohabitacion[]" class="control-label">Habitación 1)</label>
      <select name="tipohabitacion[]" id="tipohabitacion1" class="form-control listaha">
        <script>document.write(options);</script>
      </select>
    </span>

    <span class="form-horizontal col-sm-6 col-md-6 checkinout">

        
        <span class="col-md-12">
          <label for="checkin[]" class="control-label">Check-In:</label>
          <input placeholder="Ej: 01/01/2014"  style="" class="form-control calNoStyle dateIN" name="checkin[]" id="checkin1">
        </span>

        
        <span class="col-md-12">
          <label for="checkout[]" class="control-label">Check-Out:</label>
          <input placeholder="Ej: 01/01/2014"  style="" class="form-control calNoStyle dateOUT" name="checkout[]" id="checkout1">
        </span> 

    </span> <!-- Checkin and Out -->
  </div>
  <div id="habHolder"></div><span id="punteroHabs"></span>
  <span><img id="bMas" style="width:30px; height:30px; cursor:pointer;" src="/images/boton_mas.png"></span>
  <span><img id="bMenos" style="width:30px; height:30px; cursor:pointer;" src="/images/boton_menos.png"></span>

  
  <div class="form-group">
    <label for="huesped[]" class="col-xs-4 col-sm-4 control-label">Huésped(es) Habitación 1:</label>
    <div class="col-xs-8 col-sm-8">
      <input type="text" class="form-control" name="huesped[]" id="huesped1" placeholder="Ej: Juan Alfonzo y Maria Arteaga">
    </div>
  </div> <!-- Huesped(es) -->

<div id="huesHolder"></div><span id="punteroHues"></span>





<div class="form-group">
  <label for="Comentarios" class="col-xs-4 col-sm-4 control-label">Comentarios</label>
  <div class="col-xs-8 col-sm-8">
    <textarea class="form-control" name="Comentarios" id="Comentarios" placeholder="Comentarios"></textarea>
  </div>
</div> <!-- Comentarios -->

<div class="form-group">
  <div class="col-md-12">
    <button type="button" id="submitModal" class="btn btn-default btn-lg btn-block"  data-target="#myModal">Reservar</button>
  </div>
</div> <!-- Boton -->

</fieldset> <!-- Campos -->
</form> <!-- Formulario -->
</section>