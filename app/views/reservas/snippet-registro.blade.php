@if($editar)
{{ Form::open(array('url' => '/usuario/editar', 'method' => 'put', 'id' => 'formRegistro', 'class' => 'form-horizontal'))}}
@else
{{ Form::open(array('url' => '/usuario/registro', 'method' => 'post', 'id' => 'formRegistro', 'class' => 'form-horizontal'))}}
@endif

  <fieldset id="camposForm">
    <div class="form-group">
      <div class="col-md-12">
        <label for="usuario" class="">Usuario</label>
        <input type="text" class="form-control" name="usuario" id="usuario" value="@if(Auth::check()) {{ Auth::user()->identificacion }} @endif" @if($editar) {{'disabled'}} @endif>
      </div>
    </div> <!-- Usuario -->

    <div class="form-group">
      <div class="col-md-12">
        <label for="password" class="">Contraseña</label>
        <input type="password" placeholder="@if(isset($cliente)){{'Dejar en blanco para no cambiar'}}@endif" class="form-control" name="password" id="password">
      </div>
    </div> <!-- Password -->

    <div class="form-group">
      <div class="col-md-12">
        <label for="confPassword" class="">Confirmar Contraseña</label>
        <input type="password" placeholder="@if(isset($cliente)){{'Dejar en blanco para no cambiar'}}@endif" class="form-control" name="confPassword" id="confPassword">
      </div>
    </div> <!-- Confirmar Password -->

    <div class="form-group">      
        <div class="col-md-12">
          <label for="cedrif" class="">Cédula / R.I.F.</label>
          <div class="input-group">
            <div class="input-group-btn">
              <button type="button" id="tipoCed" class="btn btn-default 
              dropdown-toggle" data-toggle="dropdown">@if(isset($usuario)){{$cliente->getTipoCedu()}}@else V @endif <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
              <li><a id="Ven">V</a></li>
              <li><a id="Ex">E</a></li>
              <li><a id="Jud">J</a></li>
              <li><a id="Gob">G</a></li>
              <li><a id="Pas">P</a></li>
            </ul>
          </div><!-- /btn-group -->
          <input type="hidden" value="V" id="tipoCedu" name="tipoCedu">
          <input type="text" class="form-control" name="cedrif" id="cedrif" value="@if(isset($usuario)){{$cliente->getNumericCed()}}@endif" placeholder="ej: 305241945">
        </div>
      </div>
    </div> <!-- Cedula o rif -->

    <div class="form-group">
      <div class="col-md-12">
        <label for="nombre_rz" class="">Nombre/Razón Social</label>
        <input type="text" class="form-control" name="nombre_rz" value="@if(isset($cliente)){{$cliente->nombre_rz}}@endif" id="nombre_rz" placeholder="Ej: Palace Hotel, C.A.">
      </div>
    </div> <!-- Nombre/Razon Social -->

    <div class="form-group hidden">
      <label for="solicitante" class="col-sm-4 control-label">Solicitante</label>
      <div class="col-md-12">
        <input type="text" class="form-control" name="solicitante" id="solicitante" placeholder="Ej: Palace Hotel, C.A.">
      </div>
    </div> <!-- Solicitante -->

    <div class="form-group">
      <div class="col-md-12">
        <label for="email" class="">E-mail</label>
        <input type="text" class="form-control" value="@if(isset($usuario)){{$usuario->email}}@endif" name="email" id="email" placeholder="Email">  
      </div>
    </div> <!-- Email -->

    <div class="form-group">      
      <div class="col-md-12">
        <label for="email" class="">E-mail del encargado de reservas</label>
        <input type="text" class="form-control" value="@if(isset($cliente)){{$cliente->email}}@endif" name="emailC" id="emailC" placeholder="Email">  
      </div>
    </div> <!-- Email Reservas -->

    <div class="form-group">      
      <div class="col-md-12">
        <label for="telefono" class="">Teléfono</label>
        <input type="text" class="form-control" value="@if(isset($cliente)){{$cliente->telefono}}@endif" name="telefono" id="telefono" placeholder="Telefono">
      </div>
    </div> <!-- Telefono -->


    <div class="form-group">
      <div class="col-md-12">
        <button type="submit" id="submitModal" class="btn btn-default btn-lg btn-block">@if(isset($cliente)){{'Guardar'}}@else 'Registrar'@endif</button>
      </div>
    </div> <!-- Boton -->

  </fieldset> <!-- Campos -->

{{ Form::Close() }}