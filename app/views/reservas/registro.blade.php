<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/favicon.png" />
  <title>Palace Hotel, C.A. - Registro</title>
  {{ HTML::style('css/bootstrap.css'); }}
  {{ HTML::style('css/styles.css'); }}
</head>
<body id="registro">
  @include('header') <!-- Header -->
  <div class="container">
    <div class="content row"> 
      <section class="registroUsua col-md-6 col-md-offset-3">
        <legend><h2>Registro de usuario</h2></legend>
       @include('reservas.snippet-registro')
      </section> <!-- Articles -->
    </div> <!-- Content -->
  </div> <!-- Container -->
  @include('footer')
  {{ HTML::script('js/jquery.js'); }}
  {{ HTML::script('js/bootstrap.js'); }}
  {{ HTML::script('js/myscripts.js'); }}
  {{ HTML::script('js/bootstrapValidator.min.js'); }}

  <script type="text/javascript">
    $(document).ready(function() {
      $('#formRegistro').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
          usuario: {
            message: 'Campo no valido',
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede estar vacio.'
              },
              stringLength: {
                min: 6,
                max: 20,
                message: 'Este campo debe tener minimo 6 caracteres y un maximo de 20'
              },
              regexp: {
                regexp: /^[a-zA-Z0-9]+$/,
                message: 'Este campo solo puede contener caracteres y numeros'
	              }
	           },
	       },
	       password: {
            message: 'Campo no valido',
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede estar vacio.'
              },
              stringLength: {
                min: 8,
                max: 20,
                message: 'La contrase;a debe tener minimo 8 caracteres y un maximo de 20'
              },
	         },
	       },
	       confPassword: {
            message: 'Campo no valido',
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede estar vacio.'
              },
              stringLength: {
                min: 8,
                max: 20,
                message: 'La contrase;a debe tener minimo 8 caracteres y un maximo de 20'
              },
              identical: {
                        field: 'password',
                        message: 'La contrase;a y la confirmacion no coinciden'
                    },
	         },
	       },
          nombre_rz: {
            message: 'Campo no valido',
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede estar vacio.'
              },
              stringLength: {
                min: 6,
                max: 100,
                message: 'Este campo debe tener minimo 6 caracteres y un maximo de 100'
              },
              regexp: {
                regexp: /^[a-zA-Z0-9_. ,]+$/,
                message: 'Este campo solo puede contener caracteres, numeros y puntos'
              }
            }
          },
          email: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'El correo es requerido y no puede ir vacio'
              },
              emailAddress: {
                message: 'El texto ingresado no es una direccion de correo electronico valida.'
              }
            }
          },
         emailC: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'El correo es requerido y no puede ir vacio'
              },
              emailAddress: {
                message: 'El texto ingresado no es una direccion de correo electronico valida.'
              }
            }
          },
          telefono: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'El numero de telefono es requerido y no puede ir vacio.'
              },
              regexp: {
                regexp: /\b\d{4}\-\d{3}\-\d{4}$/,
                message: 'Por favor use el formato de 11 numeros Los guiones se agregan solos. Ej: 0235-342-2440.'
              },
              stringLength: {
                min: 13,
                max: 13,
                message:''
              },
            }
          },
          cedrif: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede ir vacio.'
              },
              regexp: {
                regexp: /^[0-9]{6,9}$/,
                message: 'Este campo puede contener entre 6 y 9 digitos'
              }
            }
          },

        }
      });
     
});

$(window).on('load', function(){
  $("html, body").animate({ scrollTop: $('#menu').offset().top }, 1000);
});






</script>
</body>
</html>
