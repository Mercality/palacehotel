<!DOCTYPE html>
<html lang="es">
<head>
<?php $s = Auth::check(); ?>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/favicon.png" />
  <title>Palace Hotel, C.A. - Servicios</title>
  {{ HTML::style('css/bootstrap.css'); }}
  {{ HTML::style('css/styles.css'); }}
  {{ HTML::style('js/jqui/jquery-ui.css'); }}
  <script>
        /*Compueba el tipo de tarifa del cliente (Coorporativa o normal)*/
        var habitacionesObj = {{ $habitaciones or 'undefined' }},
            corporativo = {{ $cliente->tarifa_corporativa or 'undefined' }};
          var options = null;
         for (var m = 0; m <= habitacionesObj.length-1; m++) {
         //console.log(habitacionesObj[m].id);
            if (!corporativo) {
              options = options+'<option value="'+habitacionesObj[m].id+'">'+habitacionesObj[m].tipo+' - Bs. '+habitacionesObj[m].tarifa+'</option>';
            } else {
              options = options+'<option value="'+habitacionesObj[m].id+'">'+habitacionesObj[m].tipo+' - Bs. '+habitacionesObj[m].tarifa_corporativa+'</option>';
            }   
          }
  </script>
</head>
<body id="reservasHabita">
@if($errores = Session::get('errores'))
      <div class="modal fade" id="smallModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Cerrar</span></button>
              <h4 class="modal-title">Error</h4>
            </div>
            <div class="modal-body">
              <p>
              @if( isset($errores['registrado']) )
              {{ 'El número de cédula: <strong>'.$errores['registrado']['cedula'].'</strong> pertenece a un usuario registrado, por favor inicie sesión. Si cree que esto es un error por favor contacte con nuestra administración y procederemos a solventarlo a la brevedad posible.' }}
              @endif
               @if( isset($errores['loginfail']))
              {{ 'No se pudo iniciar sesión debido a que los datos provistos no coinciden con nuestros registros. Intente nuevamente.' }}
              @endif
              </p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
   @endif
   <!-- MODAL CONFIRMACION DE APROBAR O RECHAZAR -->

  @include('header') <!-- Header -->
  <div class="container">
    <div class="content row"> 

       @include('reservas.snippet-formulario_reserva')

    </div> <!-- Content -->
  </div> <!-- Container -->
  @include('footer')
  {{ HTML::script('js/jquery.js'); }}
  {{ HTML::script('js/jqui/jquery-ui.min.js'); }}
  {{ HTML::script('js/jqui/datepicker-es.js'); }}
  {{ HTML::script('js/bootstrap.js'); }}
  {{ HTML::script('js/moment.min.js'); }}
  {{ HTML::script('js/accounting.min.js'); }}
  {{ HTML::script('js/language/es.js'); }}
  {{ HTML::script('js/bootstrapValidator.min.js'); }}
  {{ HTML::script('js/myscripts.js'); }}
 
  
  <script type="text/javascript">
    $(document).ready(function() {
      $('#formReserva').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'glyphicon glyphicon-ok',
          invalid: 'glyphicon glyphicon-remove',
          validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
          nombre_rz: {
            message: 'Campo no valido',
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede estar vacio.'
              },
              stringLength: {
                min: 4,
                max: 20,
                message: 'Este campo debe tener minimo 4 caracteres y un maximo de 20'
              },
              regexp: {
                regexp: /^[a-zA-Z0-9_. ,]+$/,
                message: 'Este campo solo puede contener caracteres, numeros y puntos'
              }
            }
          },
          email: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'El correo es requerido y no puede ir vacio'
              },
              emailAddress: {
                message: 'El texto ingresado no es una direccion de correo electronico valida.'
              }
            }
          },
          telefono: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'El numero de telefono es requerido y no puede ir vacio.'
              },
              regexp: {
                regexp: /\b\d{4}\-\d{3}\-\d{4}/,
                message: 'Por favor use el formato de 11 numeros Los guiones se agregan solos. Ej: 0235-342-2440.'
              },
              stringLength: {
                min: 13,
                max: 13,
                message:''
              },
            }
          },
          cedrif: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede ir vacio.'
              },
              regexp: {
                regexp: /^[0-9]{6,9}$/,
                message: 'Este campo puede contener entre 6 y 9 digitos'
              }
            }
          },
        solicitante: {
            trigger: 'blur',
            validators: {
              notEmpty: {
                message: 'Este campo es requerido y no puede ir vacio.'
              },
              stringLength: {
                min:10,
                max: 50,
                message: 'Este campo debe tener un minimo de 10 y un maximo de 50 caracteres'
              },
            }
          },
          'huesped[]': {
            trigger: 'blur',
            validators: {
              stringLength: {
                max: 50,
                message: 'Este campo debe tener un maximo de 50 caracteres'
              },
            }
          },
          'checkin[]': {
                trigger: 'change',
                group: '.cins',
                validators: {
                    date: {
                        message: 'La fecha no es valida',
                        format: 'DD/MM/YYYY'
                    },
                    callback: {
                        message: 'La fecha no puede ser anterior a hoy',
                        callback: function(value, validator) {
                            var m = new moment(value, 'DD/MM/YYYY', true);
                            if (!m.isValid()) {
                                return false;
                            }
                            // Check if the date in our range
                            return m.isAfter() || m.isSame(moment(),'day');
                        }
                    }
                }
            },
            'checkout[]': {
                trigger: 'change',
                group: '.couts',
                validators: {
                    date: {
                        message: 'La fecha no es valida',
                        format: 'DD/MM/YYYY'
                    },
                    callback: {
                        message: 'La fecha no puede ser anterior o igual a hoy',
                        callback: function(value, validator) {
                            var m = new moment(value, 'DD/MM/YYYY', true);
                            if (!m.isValid()) {
                                return false;
                            }
                            // Check if the date in our range
                            return m.isAfter() || !m.isSame(moment(),'day');
                        }
                    }
                }
            },

        }
      });
     
});
$('#formReserva').on('status.field.bv', function(e, data) {
            // $(e.target)  --> The field element
            // data.bv      --> The BootstrapValidator instance
            // data.field   --> The field name
            // data.element --> The field element

            data.bv.disableSubmitButtons(false);
        });
$(window).on('load', function(){
  $("html, body").animate({ scrollTop: $('#menu').offset().top }, 1000);
});
</script>
</body>
</html>
