<header class="">
	<div id="w1">
		<div class="container">
			<div class="row">
				<section class="logo col-md-12">
					<a href="index.php"><img class="img-responsive" src="{{asset('images/PalaceHotel.png')}}" alt="Logo"></a>
				</section> <!-- branding -->

			</div> <!-- row -->
			<br>
			<div class="row">
				<div class="btn-reserva col-md-12">
					<a href="/reserva" id="btn-Reserva" class="btn btn-lg">Reserva tu habitación</a>
				</div> <!-- Direccion -->
			</div>
		</div>
	</div>
	
	
	<div id="menu">

		<div class="container">
			<div class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="glyphicon glyphicon-th-list"></span>
					</button>
				</div> <!-- Navbar-header para moviles -->

				<nav class="collapse navbar-collapse" id="collapse">
					<ul class="nav nav-pills nav-justified">
						<li><a href="/">Inicio</a></li>
						<li><a id="servicios" href="/habitaciones#enServicios">Servicios</a></li>
						<li><a href="/habitaciones">Habitaciones</a></li>
						<li><a href="/tascarestaurant">Tasca-Restaurant</a></li>
						<li><a href="/reserva">Reservas</a></li>
						<li><a href="/#Contacto" id="contacto">Contacto</a></li>
					</ul> <!-- nav -->
				</div> <!-- Collapse -->
			</nav> <!-- navbar -->
		</div>
		
	</div>
</header>

