<div id="pre-servicios">
	<div class="container">
		<div class="row">
			<h1>Servicios</h1>
			<div class="col-md-8 col-md-offset-2 col-lg-10 col-lg-offset-1">
				<p class="text-center"><em>La  pasión por el servicio a cualquier hora...</em></p>
				<p class="">En Palace Hotel, C.A. no existen horarios para ofrecerle un buen servicio con la mejor sonrisa. Desde la mañana hasta la noche a cualquier hora estamos disponibles para ayudarle y ofrecerle los servicios que le harán pasar una confortable estadía.
				</p>
			</div>
		</div>
<br>
		<div class="row">
			<div class="col-sm-4 col-md-2 col-md-offset-2">
				<img src="{{asset('images/coffe-128.jpg')}}" alt="" class="img-responsive img-circle">
				<h4 class="text-center">Coffee-Shop</h4>
				<p><small>Café, Dulces, Sandwiches, Comida Rápida...</small></p>
			</div>

			<div class="col-sm-4 col-md-2 col-md-offset-1">
				<img src="{{asset('images/rest-128.jpg')}}" alt="" class="img-responsive img-circle">
				<h4 class="text-center">Restaurante</h4>
				<p><small>El Restaurante tiene un extenso menú de comida internacional y nacional para todos los gustos.</small></p>
			</div>

			<div class="col-sm-4 col-md-2 col-md-offset-1">
				<img src="{{asset('images/tasca-128.jpg')}}" alt="" class="img-responsive img-circle">
				<h4 class="text-center">Tasca</h4>
				<p><small>Nuestra Tasca es perfecta para realizar toda clase de eventos: bodas, bautizos, cumpleaños... Disfrutará de un ambiente íntimo y agradable para compartir con sus seres más allegados.</small></p>
			</div>
		</div>
	</div>
</div>