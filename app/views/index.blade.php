<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="description" content="Bienvenidos a Palace Hotel, C.A.. Moderno y confortable, situado en el centro de Valle de la Pascua - Venezuela a minutos de Bancos y Centros Comerciales.">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/favicon.png" />
  <title>Palace Hotel, C.A. - Valle de la Pascua</title>
  {{ HTML::style('css/bootstrap.css'); }}
  {{ HTML::style('css/styles.css'); }}
</head>
<body id="principal">
   @if(Session::get('reserva') == 'success')
      <div class="modal fade" id="smallModal">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Reserva recibida</h4>
            </div>
            <div class="modal-body">
              <p>Su reserva ha sido recibida. Cabe mencionar que esta solicitud no garantiza disponibilidad, por favor aguarde a su confirmación.</p>
              <p>Nota: Si su reserva es para el día siguiente o el mismo día, le invitamos a comunicarse telefonicamente y proveer su número de reserva, nuestras solicitudes en linea reciben atención prioritaria.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
   @endif

  @include('header')<!-- Header -->
  @include('quienesSomos')<!--Quienes Somos-->
  @include('pre-servicios')<!--Servicios-->
  @include('separador')<!--separador-->
  @include('pre-habitaciones')<!--Habitaciones-->
  @include('contactanos')<!--Contactanos-->
  @include('footer')<!--footer-->
  
  
  {{ HTML::script('js/jquery.js'); }}
  {{ HTML::script('js/myscripts.js'); }}
  {{ HTML::script('js/bootstrap.js'); }}
  <script>
    $('#contacto').on('click', function(){
      $("html, body").animate({ scrollTop: $('#contactanos').offset().top }, 2000);
    });
  </script>
</body>
</html>