<div class="container">
	<div class="row">
		<div class="jumbotron text-justify">
		  <h1>Habitaciones</h1>
		  <p>Palace Hotel, C.A. cuenta con diferentes tipos de habitación que se ajustan a su presupuesto y necesidad. Le ofrecemos 80 habitaciones con balcón distribuídas en cincos (5) pisos, diseñadas bajo las últimas tendencias arquitectónicas y la más alta tecnología acompañadas de una variada gama de servicios, seguridad y confort que harán que usted se sienta como en su casa.</p>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/matrimonial.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicos sumados...</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/matrimonial" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Doble (Matrimonial + 1 Ind.)</h4>
      			<img src="{{asset('images/habs/matri-1-individual.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación doble es ideal para un acompañante extra ya que además de contar con una cama matrimonial se adiciona una cómoda cama individual...</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/matInd" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Triple (3 camas individuales)</h4>
      			<img style="width:350px; height:140px;" src="{{asset('images/habs/triple/matrimonial1.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Tenemos a su disposición nuestra habitación de 3 camas individuales equipadas con todos nuestros servicios básicos para cuando haga una visita grupal y la economía sea su prioridad ...</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/triple" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>
	
		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Cuádruple II (2 Matrimoniales)</h4>
      			<img src="{{asset('images/habs/2matB.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación cuádruple de primer nivel le ofrece la mejor comodidad y versatilidad ya que puede alojar desde dos personas hasta un total de cuatro personas....</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/IVDobMAT" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Cuádruple III (Matrimonial + 2 Ind.)</h4>
      			<img src="{{asset('images/habs/2mat.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>La habitación preferida por las familias y grupos de viaje. Gracias a su composición de una cama matrimonial y dos individuales le permite...</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/IV2MAT2IND" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Cuádruple IV (4 Camas individuales)</h4>
      			<img src="{{asset('images/habs/4ind.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Esta habitación es ideal en caso de que viaje en grupo y necesite mayor capacidad al más bajo costo.  Equipada con todas las comodidades básicas  de nuestras habitaciones...</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/IV4IND" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>
	
		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Junior Ejecutiva</h4>
      			<img src="{{asset('images/habs/jrEjecutiva.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación ejecutiva destaca con una cama tamaño queen que le ofrecerá un insuperable confort, acompañada de una cama individual, un sofá, TV pantalla plana...</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/Junior" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Palace Ejecutiva</h4>
      			<img src="{{asset('images/habs/palace.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación estrella, ideal para esa ocasión única o si de verdad desea distinguirse. Cuenta con una cama tamaño queen, sofá, y una cama individual....</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/Palace" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Palace Familiar</h4>
      			<img src="{{asset('images/habs/palace.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>La Suite Familiar le permitirá disfrutar de lo mejor que tenemos para ofrecerle, además de los implementos que incluye una habitación Palace, le ofrecemos un agradable comedor...</p>
	        		<p class="text-right">
	        		<a href="/habitaciones/PalaceFamiliar" class="btn btn-default btn-xs" role="button">Más detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>
	</div>
</div>

