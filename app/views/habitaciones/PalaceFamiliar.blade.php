<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/favicon.png" />
  <title>Palace Hotel, C.A. - Habitaciones</title>
  {{ HTML::style('css/bootstrap.css'); }}
  {{ HTML::style('css/styles.css'); }}
</head>
<body>
      @include('header') <!-- Header -->
      
		<div class="container">
			<div class="row">
				
				<h2>Habitación Palace Familiar</h2>
				<div class="col-sm-8">
				
					<!--CAROUSEL-->
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
						    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
						  </ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <img src="{{asset('images/habs/PalaceFamiliar/palaceFamiliar-1.jpg')}}" alt="...">
						      <div class="carousel-caption">
						        Habitación Palace Familiar
						      </div>
						    </div>
						    <div class="item">
						      <img src="{{asset('images/habs/PalaceFamiliar/palaceFamiliar2.jpg')}}" alt="...">
						      <div class="carousel-caption">
						       	Habitación Palace Familiar
						      </div>
						    </div>
						    
						</div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a><!--Fin Controls -->
					</div><!--FIN CAROUSEL-->

					<br>
					<div class="row">
						<div class="col-sm-6"><p class="text-left"><a href="/habitaciones/Palace" class="btn btn-default btn-lg">Anterior</a></p></div>
						<div class="col-sm-6"></div>
					</div>
				</div>
				
					<div class="col-sm-4">
						<p class="justificado">La Suite Familiar le permitirá disfrutar de lo mejor que tenemos para ofrecerle cuando viaje con su familia. </p>
						<h3>Detalles</h3>
						<ul>
							<li>Cama Queen</li>
							<li>2 Camas Individuales</li>
							<li>Sofá</li>
							<li>Mesa Comedor</li>
							<li>Nevera Ejecutiva</li>
							<li>Aire Acondicionado</li>
							<li>Teléfono</li>							
							<li>Televisión Satelital</li>
							<li>Baño con agua fría y caliente</li>							<li class="text-warning"><strong>Precio: {{ $precio }}Bs.f</strong></li>
						</ul>
						<a href="#" class="btn btn-default btn-block">Reservar</a>
					</div>
			</div>
		</div>
		<br>

      @include('footer') <!-- Footer --> <!-- Footer -->
  
  {{ HTML::script('js/jquery.js'); }}
  {{ HTML::script('js/myscripts.js'); }}
  {{ HTML::script('js/bootstrap.js'); }}
  <script>
	$(window).on('load', function(){
  $("html, body").animate({ scrollTop: $('#menu').offset().top }, 1000);
});
</script>
</body>
</html>
