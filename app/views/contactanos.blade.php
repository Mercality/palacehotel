<div id="contactanos">
	<a name="Contacto"></a>
	<div class="container">
		<div class="row white">
			<h1>Contáctanos</h1>
			<div class="col-sm-3 col-md-3">
				<h4>Ubicación:</h4>
				<p><strong>País:</strong> Venezuela</p>
				<p><strong>Estado:</strong> Guárico</p>
				<p><strong>Ciudad:</strong> Valle de la Pascua</p>
				<p><strong>Dirección:</strong> Av. Rómulo Gallegos</p>
				<h4>Teléfonos:</h4>
				<em><a href="tel://0235-3422339">0235-3422339</a></em><span> / </span>
				<em><a href="tel://0235-3422073">0235-3422073</a></em><span> / </span>
				<em><a href="tel://0235-3422440">0235-3422440</a></em><span> / </span>
				<em><a href="tel://0235-3422259">0235-3422259</a></em>
				<p><strong>Email:</strong> palacev@gmail.com</p>

			</div>

			<div class="col-sm-9 col-md-9" id="map-canvas">
				<!--map google-->
			</div>
		</div>
	</div>
</div>
<!--GOOGLE MAPA SCRIPT-->
<script src="http://maps.googleapis.com/maps/api/js"></script>
	<script>
		var map;
		function initialize() 
		{
			var mapOptions = {
				zoom: 18,
				scrollwheel: false,
				center: new google.maps.LatLng(9.2160096, -66.0053511)
			};
			map = new google.maps.Map(document.getElementById('map-canvas'),
			mapOptions);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
