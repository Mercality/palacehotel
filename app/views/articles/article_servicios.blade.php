<section id="enServicios" class="Arservicios">
   <div class="white">
   <h1>En cada una de nuestras habitaciones  usted disfrutará de:</h1>
      <div class="row">
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Aire Acondicionado</strong></caption>
               <img src="/images/servicios/Minisplit_512.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Agua Caliente</strong></caption>
               <img src="/images/servicios/Person_enjoying_jacuzzi_hot_water_bath_512.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Estacionamiento Privado</strong></caption>
               <img src="/images/servicios/Parking_512.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Internet Wi-Fi</strong></caption>
               <img src="/images/servicios/wi_fi_sign_with_bow_512.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Servicio de Lavandería</strong></caption>
               <img src="/images/servicios/Washing_machine_512.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Servicio de Planchado</strong></caption>
               <img src="/images/servicios/Clothes_hanger_512.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Servicio de Restaurant</strong></caption>
               <img src="/images/servicios/restaurant.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
         <div class="col-md-3 col-sm-4 col-xs-12 text-center">
            <div class="thumbnail">
               <caption><strong>Televisión Satelital</strong></caption>
               <img src="/images/servicios/Radio_control_512.png" alt="Servicios" class="img-responsive">
            </div>
         </div> <!-- col -->
      
      </div> <!-- Row --></div>
</section> <!-- Article Servicios -->