<div class="container">
	<div class="row">
		<div class="jumbotron text-justify">
		  <h1>Habitaciones</h1>
		  <p>PALACE Hotel, C.A. cuenta con diferentes tipos de habitaciones que se ajustan a su presupuesto y necesidad. Le ofrecemos 80 habitaciones con balcón distribuidas en cincos (5) pisos, diseñadas bajo las últimas tendencias arquitectónicas y la más alta tecnología acompañadas de una variada gama de servicios, seguridad y confort que harán que usted se sienta como en su casa!</p>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="/articles/article_hab_matrimonial" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>
	
		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>
	
		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>

  		<div class="col-sm-6 col-md-4">
    		<div class="img-habitacion thumbnail">
    			<h4>Matrimonial</h4>
      			<img src="{{asset('images/habs/300x100.jpg')}}" alt="foto habitacion">
      			<div class="caption">
					<p>Nuestra habitación matrimonial es perfecta para una pareja o si usted esta en un viaje de negocios, cuenta con todos nuestros servicios básicossumados...</p>
	        		<p class="text-right">
	        		<a href="#" class="btn btn-default btn-xs" role="button">Mas detalles</a>
	        		</p>
	      		</div>
    		</div>
  		</div>
	</div>
</div>