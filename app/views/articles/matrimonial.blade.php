<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/favicon.png" />
  <title>Palace Hotel, C.A. - Habitaciones</title>
  {{ HTML::style('css/bootstrap.css'); }}
  {{ HTML::style('css/styles.css'); }}
</head>

    @include('header') <!-- Header -->
    <article>  
		<div class="container">
			<div class="row">
				
				<h2>Habitacion Matrimonial</h2>
				<div class="col-md-8">
				
					<!--CAROUSEL-->
					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						  <!-- Indicators -->
						  <ol class="carousel-indicators">
						    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
						    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
						  </ol>

						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						    <div class="item active">
						      <img src="{{asset('images/habs/mat/matrimonial.jpg')}}" alt="...">
						      <div class="carousel-caption">
						        Habitacion Matrimonial
						      </div>
						    </div>
						    <div class="item">
						      <img src="{{asset('images/habs/mat/matrimonial.jpg')}}" alt="...">
						      <div class="carousel-caption">
						        ...
						      </div>
						    </div>
						    
						  </div>

						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
						    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
						    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
						  </a><!--Fin Controls -->
					</div><!--FIN CAROUSEL-->
				</div>
				
					<div class="col-md-4">
					<p class="">Nestra habitacion matrimonial es perfecta para una pareja o si usted esta en un vieaje de negocios, cuenta con todos nuestros servicios báscios sumados a una excelente vista con balcón</p>
					<h3>Detalles</h3>
					<ul>
						<li>Cama Matrimonial</li>
						<li>Mini Escritorio</li>
						<li>Aire Acondicionado</li>
						<li>Telefono</li>
						<li>Televisión por cable</li>
						<li>Baño con agua fria y caliente</li>
						<li>Vista Avenida</li>
						<li class="text-warning"><strong>Precio: 1550Bs.f</strong></li>
					</ul>
					</div>
			</div>
		</div>
	</article>
	<br>

     @include('footer') <!-- Footer --> <!-- Footer -->
  
  {{ HTML::script('js/jquery.js'); }}
  {{ HTML::script('js/myscripts.js'); }}
  {{ HTML::script('js/bootstrap.js'); }}
</body>
</html>
