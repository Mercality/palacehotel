<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Palace Hotel, C.A.</title>
</head>
<body>
	<h1>Notificación de Solicitud de Reserva Recibida</h1>
	<p>Su solicitud de reserva bajo el número: <strong>{{$solicitud->numeroSolicitud()}}</strong> ha sido recibida y está en proceso de revisión por nuestro departamento de reservas. Por favor aguarde a su confirmación.
	<p>Nota: Si su reserva es para el día siguiente o el mismo día, le invitamos a comunicarse telefonicamente y proveer su número de reserva, nuestras solicitudes en linea reciben atención prioritaria.</p>
</body>
</html>
