<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Reestablecer la contraseña de su cuenta en Palace Hotel, C.A.</h2>

		<div>
			Para reestablecer su contraseña por favor haga clic en el siguiente link y complete el formulario: {{ URL::to('password/reset', array($token)) }}.<br/>
			Este link expirará en {{ Config::get('auth.reminder.expire', 60) }} minutos.
		</div>
	</body>
</html>
