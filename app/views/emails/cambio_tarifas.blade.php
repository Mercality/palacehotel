<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Palace Hotel, C.A.</title>
</head>
<style>
table, td, th {
    border: 1px solid grey;
}

th {
    background-color: blue;
    color: white;
}
.tarifa {
	text-align: right;
	width: 100px;
}
</style>
<body>
	<h1>Notificación de Cambio de Tarifas por Servicio de Hospedaje</h1>
	<p>Estimada clientela, nos complace informarles que a partir del dia de hoy {{$fecha->format('d-m-Y')}} aplicarán nuevas tarifas por servicio
	de hospedaje, las cuales están descritas a continuación:</p>

	<table class="tabla">
		<th>Tipo de Habitación</th>
		<th>Tarifa</th>
		@foreach($habitaciones as $habitacion)
			<tr>
				<td>{{$habitacion->tipo}}</td>
				<td class="tarifa">{{$habitacion->tarifa}}</td>
			</tr>
		@endforeach
	</table>
	<p>Palace Hotel, C.A. se complace en ser su opción de hospedaje en Valle de la Pascua, esperamos recibirle y ofrecerle toda la atención que se merece para que su estadía sea lo más placentera posible. Le invitamos a visitar nuestra página web <a href="http://www.palacehotel.com.ve">www.palacehotel.com.ve</a> donde podrá conocer más a fondo nuestras instalaciones y servicios. <br><br>Atentamente, <br>La Gerencia</p>
</body>
</html>
