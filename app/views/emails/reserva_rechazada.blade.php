<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Palace Hotel, C.A.</title>
</head>
<body>
	<h1>Notificación de Reserva Rechazada</h1>
	<p>Su solicitud de reserva  bajo el número: <strong>{{$solicitud->numeroSolicitud()}}</strong> ha sido cancelada /rechazada por el siguiente motivo:
	<ul>
		<li>
			<strong>{{$solicitud->comentarios}}</strong>
		</li>
	</ul>
	Para mayor información comuníquese con nosotros a través de los números telefónicos: 0235-3422440 / 3422073 / 3422259 / 3422339 o a nuestros correos palacev@gmail.com  / reservas@palacehotel.com.ve</p>
	<h3>Normas Generales</h3>
	<ul>
		<li>No se aceptan tarjetas de crédito como garantía.</li>
		<li>La Hora del check-in es a partir de la 01:00 p.m.</li>
		<li>Las transferencias y/o depósitos deben hacerse con  mínimo 48 horas de antelación.</li>
		<li><strong>Si por algún motivo no puede asistir el día del check-in, debe llamar con 48 horas de antelación de lo contrario se cobrará como "No Show"</strong></li>
		<li>El Restaurant funciona de lunes a viernes de 06:00 a.m. a 09:30 p.m. y sábados de 06:00 a.m. a 12:00 m.</li>
		<li>INFORMACION DE PAGO: Una vez recibida la confirmación de su reserva por correo electrónico, podrá proceder a realizar el pago de su hospedaje a
través de los números de cuenta:
	<ul>
	<li><strong>Bancaribe Cuenta Corriente Nº 01140420114200079901</strong></li>
	
	<li><strong>Banco Exterior Cuenta Corriente Nº 01150066863000168090</strong></li>
	
	<li><strong>Banco Nacional de Crédito (BNC) Cuenta de Ahorro Nº 01910042161142000180</strong></li>
	</ul>
</li>
	</ul>
	<p>Palace Hotel, C.A. se complace en ser su opción de hospedaje en Valle de la Pascua, esperamos recibirle y ofrecerle toda la atención que se merece para que su estadía sea lo más placentera posible. Le invitamos a visitar nuestra página web <a href="http://www.palacehotel.com.ve">www.palacehotel.com.ve</a> donde podrá conocer más a fondo nuestras instalaciones y servicios. <br /><br />Atentamente, <br />La Gerencia</p>
</body>
</html>
