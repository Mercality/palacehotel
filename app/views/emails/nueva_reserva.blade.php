<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Palace Hotel, C.A.</title>
</head>
<body>
	<h1>Notificación de Solicitud de Reserva</h1>
	<p>El cliente: {{$solicitud->cliente->nombre_rz}} <br />
	R.I.F.: {{$solicitud->cliente->ced_rif}} <br />
	ha hecho una solicitud de reserva bajo el numero: {{$solicitud->numeroSolicitud()}}</p>
	<p>Para revisarla por favor ingrese al panel administrativo en http://www.palacehotel.com.ve/admin</p>
</body>
</html>