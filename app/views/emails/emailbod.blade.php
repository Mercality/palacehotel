<!doctype html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Palace Hotel, C.A.</title>
</head>
<body>
	<h1>Notificación de Solicitud de Reserva Recibida</h1>
	<p>Su solicitud de reserva y presupuesto bajo el número: <strong>000000</strong> ha sido recibida y está en proceso de revisión por nuestro departamento de reservas. <br>Esta solicitud no representa un compromiso de reservación por parte de nosotros hasta que haya sido revisada y aprobada. Si su reserva no ha sido confirmada en un lapso menor a 72 horas le invitamos a comunicarse con nosotros a traves de nuestros números telefónicos: 0235-3422440 / 3422073 / 3422259 / 3422259 Para verificar disponibilidad.</p>
	<h3>Normas Generales</h3>
	<ul>
		<li>No se aceptan tarjetas de crédito como garantía.</li>
		<li>La Hora del check-in es a partir de la 01:00 p.m.</li>
		<li>Las transferencias y/o depósitos deben hacerse con  mínimo 48 horas de antelación.</li>
		<li>Si por algún motivo no puede venir debe llamar con 48 horas de antelación de lo contrario se cobrará como "No Show"</li>
		<li>El Restaurant funciona de lunes a viernes de 06:00 a.m. a 09:30 p.m. y sábados de 06:00 a.m. a 12:00 m.</li>
	</ul>
	<p>Palace Hotel, C.A. se complace en ser su opción de hospedaje en Valle de la Pascua, esperamos recibirle y ofrecerle toda la atención que se merece para que su estadía sea lo mas placentera posible. Le invitamos a visitar nuestra página wwww.palacehotel.com.ve donde podrá conocer mas a fondo nuestras instalaciones y servicios.br <br><br>Atentamente <br>La Gerencia</p>
</body>
</html>
