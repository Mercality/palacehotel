<div id="separador">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 hidden-xs">
				<img src="{{asset('images/logo.png')}}" alt="">
				<p class=""><em>
					"Ese momento en el que entras en tu habitación de hotel, cierras la puerta, y sabes que ahí hay un secreto, un lujo, una fantasía. Que hay consuelo. Que hay tranquilidad..."</em>
				</p>
			</div>
		</div>
	</div>
</div>