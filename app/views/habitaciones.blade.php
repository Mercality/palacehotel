<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/favicon.png" />
  <title>Palace Hotel, C.A. - Habitaciones</title>
  {{ HTML::style('css/bootstrap.css'); }}
  {{ HTML::style('css/styles.css'); }}
</head>
<body id="listHab">
      @include('header') <!-- Header -->
      @include('habitaciones.article_habitaciones')
      <div class="container">@include('articles.article_servicios')</div>
      @include('footer') <!-- Footer --> <!-- Footer -->
  
  {{ HTML::script('js/jquery.js'); }}
  {{ HTML::script('js/myscripts.js'); }}
  {{ HTML::script('js/bootstrap.js'); }}

  <script>
      $('#servicios').on('click', function(){
      $("html, body").animate({ scrollTop: $('#enServicios').offset().top }, 2000);
    });

  </script>
</body>
</html>
